<?php
setlocale(LC_ALL, "fr_FR");

if (PHP_SAPI == 'cli-server') {
    // To help the built-in PHP dev server, check if the request was actually for
    // something which should probably be served as a static file
    $url  = parse_url($_SERVER['REQUEST_URI']);
    $file = __DIR__ . $url['path'];
    if (is_file($file)) {
        return false;
    }
}

require __DIR__ . '/../vendor/autoload.php';

session_start();

// Instantiate the app
$settings = require __DIR__ . '/../src/settings.php';
$app = new \Slim\App($settings);



// Set up dependencies
require __DIR__ . '/../src/dependencies.php';

// Register middleware
//require __DIR__ . '/../src/middleware.php';

// Register routes
require __DIR__ . '/../src/routes.php';

// Set up common
//require __DIR__ . '/../src/common.php';

$app->add(function ( $request,  $response, $next) {
    if ($request->getMethod() !== 'OPTIONS' || php_sapi_name() === 'cli') {
        $response = $next($request, $response);
        //$response = $response->withStatus(301);
        return $response;
    }
    $response = $next($request, $response);
    $response = $response->withHeader('Access-Control-Allow-Origin', '*');
    $response = $response->withHeader('Access-Control-Allow-Methods', $request->getHeaderLine('Access-Control-Request-Method'));
    $response = $response->withHeader('Access-Control-Allow-Headers', $request->getHeaderLine('Access-Control-Request-Headers'));
    return $response;
});

$container = $app->getContainer();
$settings = $container->get('settings');


// $app->add(new \Tuupola\Middleware\JwtAuthentication([
//     "ignore" => ["/login", "/hello", "/forgotPassword"],
//     "secret" => $settings['jwt']['privateKey'],
//     "algorithm" => [$settings['jwt']['algorithm']],
//     "error" => function ($response, $arguments) {
//         $data["status"] = "error";
//         $data["message"] = $arguments["message"];
//         return $response
//             ->withHeader("Content-Type", "application/json")
//             ->write(json_encode($data, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));
//     }
// ]));

// Run app
$app->run();
