<?php
// Application middleware

// e.g: $app->add(new \Slim\Csrf\Guard);
$app->add(new \Tuupola\Middleware\JwtAuthentication([
    //"path" => "/api", /* or ["/api", "/admin"] */
    //"attribute" => "decoded_token_data",
    "secret" => "37LvDSm4XvjYOh9Y",
    "algorithm" => ["HS256"],
    "error" => function ($response, $arguments) {
        $data["status"] = "error";
        $data["message"] = $arguments["message"];
        return $response
            ->withHeader("Content-Type", "application/json")
            ->write(json_encode($data, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));
    }
]));



	
//$app->add('App\Middlewares\AppMiddleware:run');
//$app->add('App\Middlewares\Auth');

// detection de la langue du browser dexec
// $app->add(function (Request $request, Response $response, $next) {
//     $language = $request->getHeader('accept-language')[0];
//     $language = explode(',', $language)[0];
//     $language = explode('-', $language)[0];

//     if ($language !== 'fr' && $language !== 'en') {
//         // default
//         $language = 'en';
//     }

//     $request = $request->withAttribute('accept-language', $language);

//     return $next($request, $response);
// });