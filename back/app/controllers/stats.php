<?php

namespace App\Controllers;

use \Firebase\JWT\JWT as JWT;
use \App\Controllers\Common as Common;
use \App\Controllers\Misc as misc;
use \Ovh\Api;
use DateTime;


class Stats 
{
    private $app;
    public function __construct($app)
    {
        $this->app = $app;
    } 

    //////////////////////////////////////////////////////////////////////////
    // statistique globals et aggregée call + message par date
    public function statsCallsAndMessagesByDate ($request, $response, $args) {
        $settings = $this->app->settings;
        $from = $request->getQueryParam('from');// creationDatetime.from
        $to = $request->getQueryParam('to');// creationDatetime.to
        $numero = $request->getQueryParam('numero'); // filtre final sur le numero de telephone
        $member = $request->getQueryParam('member'); // filtre unique (boolean) sur les membres apelants
        $sviopen = $request->getQueryParam('sviopen'); // filtre unique (boolean) sur les horaires d'ouverture du svi

        $json = [];
        $liste_full = [];
        $json['request']['from'] = $from;
        $json['request']['to'] = $to;
        $json['request']['numero'] = $numero;
        $json['request']['member'] = $member;
        $json['request']['sviopen'] = $sviopen;

        // gestion de l'ouverture du bureau
        $json['officeSchedule'] = [];
        $sql = 'SELECT * FROM `telephony_conditiontime` WHERE `datetimeFrom` >= :from AND `datetimeTo` <= :to ORDER BY `date` ASC';
        $sth = $this->app->db->prepare($sql);
        $sth->bindParam("from", $from);
        $sth->bindParam("to", $to);
        $sth->execute();
        $opens = $sth->fetchAll();

        $liste_open = array_reduce($opens, function ($accumulator, $currentValue){
            if(!$accumulator[$currentValue['date']]){
                $accumulator[$currentValue['date']]= [];
            }
            array_push($accumulator[$currentValue['date']], $currentValue);
            return $accumulator;
        });

        $json['officeSchedule'] = $liste_open;
        // fin gestion horaires du bureau

        // gestion des appels
        $sql = 'SELECT * FROM `telephony_day_calls` WHERE `creationDatetime` >= :from AND `creationDatetime` <= :to ORDER BY creationDatetime ASC';
        $sth = $this->app->db->prepare($sql);
        $sth->bindParam("from", $from);
        $sth->bindParam("to", $to);
        $sth->execute();
        $calls = $sth->fetchAll();

        $listeCallsId = Array();
        $listeCallsIdSvi = Array();

        foreach ($calls as &$c) {
            $key = (new DateTime($c['creationDatetime']))->format('YmdHis') .'-'.$c['called'].'-'.$c['calling'];
            //if(!in_array($key, $listeCallsIdSvi)){
                array_push($liste_full, 
                    Array(
                        "key" => $key,
                        "creationDatetime" => $c['creationDatetime'],
                        "type" => "call",
                        "num_from" => $c['calling'],
                        "num_to" => $c['called'],
                        "duration" => $c['duration'],
                        "isMember" => $c['member'],
                        "wayType" => $c['wayType']
                    )
                );
            //}

            // creation des clefs mais on retire les entrants du svi
            if(!in_array($key, $listeCallsId)){
                if($c['ligne'] != '0033972218570'){
                    array_push($listeCallsId,$key);
                } else {
                    array_push($listeCallsIdSvi,$key);
                }
                
            }
            //}
        }

        // Il faut dedoublonner tableau full a partir des clé
        $ids = array_column($liste_full, 'key');
        $ids = array_unique($ids);
        $liste_full = array_filter($liste_full, function ($key, $value) use ($ids) {
            return in_array($value, array_keys($ids));
        }, ARRAY_FILTER_USE_BOTH);

        //$json['liste_full'] = $liste_full;
        //$json['listeCallsId'] = $listeCallsId;

        // gestion des messages
        $sql = 'SELECT * FROM `telephony_day_messages` WHERE `creationDatetime` >= :from AND `creationDatetime` <= :to ORDER BY creationDatetime ASC';
        $sth = $this->app->db->prepare($sql);
        $sth->bindParam("from", $from);
        $sth->bindParam("to", $to);
        $sth->execute();
        $messages = $sth->fetchAll();

        foreach ($messages as &$c) {
            array_push($liste_full, 
                Array(
                    "creationDatetime" => $c['creationDatetime'],
                    "type" => "message",
                    "num_from" => $c['caller'],
                    "num_to" => $c['ligne'],
                    "duration" => $c['duration']
                )
            );
        }

        // on classe le tableau
        usort($liste_full, function ($a, $b){
            $a_timestamp = strtotime($a['creationDatetime']);
            $b_timestamp = strtotime($b['creationDatetime']);
    
            return $a_timestamp <=> $b_timestamp;
        });

        // traitement des lignes ordonnées!!!
        $listeCallsCustom = Array();
        $tel1;
        $tel2;
        $duration1;
        $duration2;
        $idLinePrevious = 0;
        $loop = 0;
        $time = 0;
        $wait = 0;
        $vCustom = array();

        //$includeListeCallsId = [];


        foreach ($liste_full as $i=>$v) {
            $vCustom = $v;
            $vCustom['i'] = $i;
            $vCustom['id'] = (new DateTime($v['creationDatetime']))->format('YmdHis') .'-'.$v['num_to'].'-'.$v['num_from'];
            //$vCustom['include'] = false;
            $vCustom['include'] = in_array($vCustom['id'], $listeCallsId);
            //$vCustom['fox'] = in_array($vCustom['id'], $listeCallsId);
            //array_push($includeListeCallsId,$vCustom['id']. ' -> '.$vCustom['include']);
            
            // test de presence en horaire d'ouverture
            $jour = (new DateTime($v['creationDatetime']))->format('Y-m-d');
            $officeSchedule = $liste_open[$jour];
            $officeIsOpen = false;
            if(sizeof($officeSchedule)> 0){
                foreach ($officeSchedule as $i=>$o) {
                    $from = (new DateTime($o['datetimeFrom']))->format('His');
                    $to = (new DateTime($o['datetimeTo']))->format('His');
                    $call = (new DateTime($v['creationDatetime']))->format('His');
                    //$var .= 'call:'.$call.' / from:'.$from.' / to:'.$to;
                    if($call >= $from && $call <= $to && !$officeIsOpen){
                        $officeIsOpen = true;
                    }
                }
            }

            $vCustom['officeIsOpen'] = $officeIsOpen;

            
            
            if($v['type'] == 'message'){
                $vCustom['final'] = 'repondeur';
                $vCustom['bg'] = 'table-warning';
                $vCustom['nbLoop'] = '';
                $vCustom['newCall'] = 'no';
                $vCustom['state'] = 'message';
                $vCustom['hangupAfterSeconds'] = '';
            }
            else if($v['type'] == 'call' && $v['wayType'] == 'outgoing'){
                $vCustom['final'] = 'outgoing';
                $vCustom['bg'] = 'table-secondary';
                $vCustom['state'] = '';
                $vCustom['nbLoop'] = '';
                $vCustom['hangupAfterSeconds'] = '';
            }
            else if($v['type'] == 'call' && $v['wayType'] != 'outgoing'){
                $vCustom['state'] = 'start';
                $vCustom['nbLoop'] = 0;
                $vCustom['hangupAfterSeconds'] = 0;
                $vCustom['bg'] = '';
                $newCall = true;
                $vCustom['newCall'] = $newCall;
                $vCustom['final'] = '';

                if($v['duration'] > 0){ // pris direct = bien ou delta d'appel de 300s
                    $vCustom['final'] = 'call_direct';
                    $vCustom['bg'] = 'table-success';
                    $newCall = true;
                    $vCustom['newCall'] = $newCall;
                }
                
                if($tel1 == $v['num_from']){ // le numero est deja en boucle en n-1, on compte le nombre de fois
                    $loop++;
                    $delta = strtotime($v['creationDatetime']) - $time; // TODO
                    if($delta > 60){ // le premier tour arrive 60sec apres l'appel = nouvel appel = DIRECT IN
                        $wait = 0;
                        $loop = 0;
                        $newCall = true;
                        $vCustom['newCall'] = $newCall; 
                        if($v['duration'] == 0){ // appel non pris apres le delta de 60"
                            $vCustom['hangupAfterSeconds'] = $wait;
                            $vCustom['final'] = '';//'appel_entrant_delta>60+duration=0';
                        } else { // appel pris apres le delai de 60"
                            $vCustom['state'] = 'start';
                            $vCustom['final'] = 'call_direct';
                            $vCustom['hangupAfterSeconds'] = $wait;
                        }
                    } else {
                        $wait += $delta;
                        // on peut enregistrer l'etat du comptage ici
                        $vCustom['state'] = 'transfert';
                        $vCustom['start'] = 'loop';
                        $vCustom['nbLoop'] = $loop;
                        $newCall = false;
                        $vCustom['newCall'] = $newCall;
                        $vCustom['hangupAfterSeconds'] = $wait;
                        if($v['duration'] > 0 && $newCall == false){
                            $vCustom['bg'] = 'table-info';
                            $vCustom['final'] = 'call_transfert';
                            $vCustom['hangupAfterSeconds'] = $wait;
                            if($loop == 0){ // si la boucle est a 0 = rappel juste apres avoir raccroché -> direct
                                $vCustom['final'] = 'call_direct';
                                $vCustom['bg'] = 'table-success';
                                $wait = 0; // on reinit le temps d'attente de decrochage
                                $vCustom['hangupAfterSeconds'] = $wait;
                                $newCall = true;
                                $vCustom['newCall'] = $newCall;
                            }
                        }  
                    }
                // } else if ($tel2 == $v['num_from']) { // on test la position du numero en n-2
                //     $vCustom['final'] = 'le n-2 est identique';
                } else { //if (tel1 != v.calling) {
                    $tel1 = $v['num_from'];
                    $tel2 = $v['num_from'];
                    $duration2 = $v['duration'];
                    $wait = 0;
                    $loop = 0;
                }
                $time = strtotime($v['creationDatetime']);

                unset($vCustom['destinationType']);
                unset($vCustom['designation']);
                unset($vCustom['countrySuffix']);
                unset($vCustom['planType']);
                unset($vCustom['priceWithoutTax']);

                // verification sur l'appel non répondu
                /*if($loop > 0 && $v['duration'] == 0){ // la duration est nulle + tour de boucle > 0
                    $idLinePrevious = $i; 
                    //$vCustom['final'] = $i;
                } else if ($loop == 0 && $newCall){ // c'est un nouvel appel
                    //if($idLinePrevious > 0 && ($i - $idLinePrevious) == 1){ // le noeud 0 ne peux pas etre un echec ! il a au moijs tourner une fois
                    if($idLinePrevious > 0){
                        $listeCallsCustom[$idLinePrevious]['final'] = 'call_fail';
                        $listeCallsCustom[$idLinePrevious]['bg'] = 'table-danger';
                    }
                }*/
            }
            array_push($listeCallsCustom, $vCustom);
        };

        //$json['includeListeCallsId'] = $includeListeCallsId;
        //$json['listeCallsCustom'] = $listeCallsCustom;

        // traitement des lignes poiur la gestion des messages
        $idLinePrevious = 0;
        foreach ($listeCallsCustom as $i=>$v) {
            if($v['type'] == "message"){ // le type de l'occurence est un message !
                $idLinePrevious = $i; // l'id precédent est enregistré
            }

            if($v['include'] == false && $v['type'] == 'call'){
                $listeCallsCustom[$i]['final'] = 'call_svi_fail';
                $listeCallsCustom[$i]['bg'] = 'table-primary';
            }

            // l'appel courant est nouveau ET le precedent nas pas de duration ET a fait plus d'nu tour
            if($v['newCall'] && $listeCallsCustom[$i -1]['duration'] == 0 && $listeCallsCustom[$i -1]['nbLoop'] > 0){
                $listeCallsCustom[$i - 1]['final'] = "call_fail";
                $listeCallsCustom[$i - 1]['bg'] = "table-danger";
            }

            // correctif pour les appels pris en transfert qui on le tour 0 en svi_fail !
            if($v['state'] == "transfert" && $listeCallsCustom[$i -1]['num_from'] == $v['num_from']){
                if($listeCallsCustom[$i - 1]['nbLoop'] == 0){ 
                    $listeCallsCustom[$i - 1]['final'] = "";
                    $listeCallsCustom[$i -1]['bg'] = '';
                }
            }

            if($idLinePrevious > 0 && ($i - $idLinePrevious) == 1){
                $j = $idLinePrevious - 1;
                $listeCallsCustom[$j]['bg'] = 'table-danger';
                if($listeCallsCustom[$j]['nbLoop'] == 0){
                    $listeCallsCustom[$j]['final'] = "message_svi_fail";
                } else {
                    $listeCallsCustom[$j]['final'] = "message_call_fail";
                }
            }

            if($listeCallsCustom[sizeof($listeCallsCustom) - 1]['type'] == "message"){
                if($listeCallsCustom[sizeof($listeCallsCustom) - 2]['nbLoop'] == 0){ // message direct du svi
                    $listeCallsCustom[sizeof($listeCallsCustom) - 2]['final'] = "message_svi_fail";
                } else {
                    $listeCallsCustom[sizeof($listeCallsCustom) - 2]['final'] = "message_call_fail";
                }
            }
        };

        // gestion de la derniere ligne du tableau
        if($listeCallsCustom[(sizeof($listeCallsCustom)-1)]['duration'] == 0 && $listeCallsCustom[(sizeof($listeCallsCustom)-1)]['nbLoop'] > 0){ // c'est un transfert non répondu !
            $listeCallsCustom[(sizeof($listeCallsCustom)-1)]['final'] = "call_fail";
            $listeCallsCustom[(sizeof($listeCallsCustom)-1)]['bg'] = 'table-danger';
        }  else if($$listeCallsCustom[(sizeof($listeCallsCustom)-1)]['duration'] > 0 && $listeCallsCustom[(sizeof($listeCallsCustom)-1)]['nbLoop'] > 0){
            $listeCallsCustom[(sizeof($listeCallsCustom)-1)]['final'] = 'call_transfert';
            $listeCallsCustom[(sizeof($listeCallsCustom)-1)]['bg'] = 'table-info';
        }

        $json['datas'] = $listeCallsCustom;

        // traitement des filtres
        if($numero){ // sur un numero
            $listeCallsCustom_filterNumero =  array_filter ($listeCallsCustom, function($el) use ($numero) {
                return $el['num_to'] == $numero;
            });
            $json['datas'] = $listeCallsCustom_filterNumero;
        }

        $call_direct =  array_filter ($listeCallsCustom, function($el) use ($numero) {
            return $el['final'] == "call_direct";
        });

        if($sviopen){
            $listeCallsCustom_filterSvi =  array_filter ($json['datas'], function($el) use ($sviopen) {
                return $el['officeIsOpen'] == $sviopen;
            });
            $json['datas'] = $listeCallsCustom_filterSvi;
        }

        // comptage des appels
        function countFinal($code, $listeCallsCustom){
            $call =  array_filter ($listeCallsCustom, function($el) use ($code) {
                return $el['final'] == $code;
            });
            return sizeof($call);
        }


        // gestion des données statistiques
        $json['stats']['call_ring_but_no_hangup'] = countFinal('', $json['datas']);
        $json['stats']['call_direct'] = countFinal('call_direct', $json['datas']);
        $json['stats']['call_transfert'] = countFinal('call_transfert', $json['datas']);
        $json['stats']['call_fail'] = countFinal('call_fail', $json['datas']) + countFinal('message_call_fail', $json['datas']);
        $json['stats']['call_svi_fail'] = countFinal('call_svi_fail', $json['datas']);
        $json['stats']['total_call'] = countFinal('call_direct', $json['datas']) + countFinal('call_transfert', $json['datas']) + countFinal('call_fail', $json['datas']) + countFinal('message_call_fail', $json['datas']) + countFinal('message_svi_fail', $json['datas']) + countFinal('call_svi_fail', $json['datas']);

        $json['stats']['message_svi_fail'] = countFinal('message_svi_fail', $json['datas']);
        $json['stats']['message_call_fail'] = countFinal('message_call_fail', $json['datas']);
        $json['stats']['total_messages'] = countFinal('message_svi_fail', $json['datas']) + countFinal('message_call_fail', $json['datas']);

        return $this->app->response->withJson($json)->withHeader('Access-Control-Allow-Origin', '*');
    }


    //////////////////////////////////////////////////////////////////////////
    // les appels par lignes
    public function statsByDate ($request, $response, $args) {
        $settings = $this->app->settings;
        // pathParam
        $date = $args['date']; // au format YYYY-MM-DD
        $line = $args['line'];

        $stats = [];

        if(strlen($date) == 10){ // YYYY-MM-DD
            $sql = 'SELECT * FROM `telephony_day` WHERE `numero` = :line AND `date` LIKE :date';
            $id_day = $day->id;
        } else { // YYYY-MM ou YYYY
            $sql = 'SELECT SUM(total_svi) AS total_svi, SUM(total_appels) AS total_appels, SUM(total_conversation) AS total_conversation, SUM(appels_ok) AS appels_ok, SUM(appels_ko) AS appels_ko, SUM(appels_membres) AS appels_membres, SUM(appels_externes) AS appels_externes, MIN(heure_first_call) AS heure_first_call, MAX(heure_last_call) AS heure_last_call FROM `telephony_day` WHERE `numero` = :line AND `date` LIKE :date AND `total_appels` != "00:00"';
        }
        
        $sth = $this->app->db->prepare($sql);
        $sth->bindValue("date", '%'.$date.'%');
        $sth->bindParam("line", $line);
        $sth->execute();
        $day = $sth->fetchAll();

        $stats['day'] = $day[0];
        $stats['day']['number'] = $line;

        if(strlen($date) == 10){ // YYYY-MM-DD
            
            $id_day = $day[0]['id'];
        } else { // YYYY-MM ou YYYY
            $sth = $this->app->db->prepare('SELECT id FROM `telephony_day` WHERE `numero` = :line AND `date` LIKE :date AND `total_appels` != "00:00"');
            $sth->bindValue("date", '%'.$date.'%');
            $sth->bindParam("line", $line);
            $sth->execute();
            $days = $sth->fetchAll();

            $array_day = [];
            foreach ($days as &$d) {
                array_push($array_day, $d['id']);
            }
            $id_day = implode(',',$array_day);
        }

        //echo '----->'.$id_day;

        // SELECT heure, SUM(total_appels) AS total_appels FROM `telephony_hours` GROUP BY `heure`
        // 

        $sql = 'SELECT heure, SUM(total_appels) AS total_appels, SUM(total_conversation) AS total_conversation, SUM(appels_ok) AS appels_ok, SUM(appels_ko) AS appels_ko, SUM(appels_membres) AS appels_membres, SUM(appels_externes) AS appels_externes FROM `telephony_hours` WHERE id_day IN ('.$id_day.') GROUP BY `heure` ORDER BY heure ASC';
        //echo $id_day;
        //echo $sql;
        $sth = $this->app->db->prepare($sql);
        //$sth->bindParam("id_day", $id_day);
        $sth->execute();
        $data = $sth->fetchAll();
        $stats['data'] = $data;
            
        return $this->app->response->withJson($stats)->withHeader('Access-Control-Allow-Origin', '*');
    }

    //////////////////////////////////////////////////////////////////////////
    // les appels par lignes en fonction d'un array de date
    public function statsByMultiDate ($request, $response, $args) {
        $settings = $this->app->settings;
        // pathParam
        $line = $args['line'];

        // parsing du body
        $input = $request->getParsedBody();
        $date = str_replace(' ', '',$input['dates']);

        $array_dates = explode(",", $date);

        $stats = [];

        $dates_in = '"'.join("\",\"",$array_dates).'"';
        //echo $dates_in;

        // if(strlen($date) == 10){ // YYYY-MM-DD
        //     $sql = 'SELECT * FROM `telephony_day` WHERE `numero` = :line AND `date` LIKE :date';
        //     $id_day = $day->id;
        // } else { // YYYY-MM ou YYYY
            $sql = 'SELECT SUM(total_svi) AS total_svi, SUM(total_appels) AS total_appels, SUM(total_conversation) AS total_conversation, SUM(appels_ok) AS appels_ok, SUM(appels_ko) AS appels_ko, SUM(appels_membres) AS appels_membres, SUM(appels_externes) AS appels_externes, MIN(heure_first_call) AS heure_first_call, MAX(heure_last_call) AS heure_last_call FROM `telephony_day` WHERE `numero` = :line AND `date` IN ('.$dates_in.') AND `total_appels` != "00:00"';
        //}
        
        $sth = $this->app->db->prepare($sql);
        $sth->bindParam("line", $line);
        $sth->execute();
        $day = $sth->fetchAll();

        $stats['day'] = $day[0];
        $stats['day']['number'] = $line;

        //print_r($stats);

        // if(strlen($date) == 10){ // YYYY-MM-DD
            
        //     $id_day = $day[0]['id'];
        // } else { // YYYY-MM ou YYYY
            $sth = $this->app->db->prepare('SELECT id FROM `telephony_day` WHERE `numero` = :line AND `date`  IN ('.$dates_in.') AND `total_appels` != "00:00"');
            // $sth->bindValue("date", '%'.$date.'%');
            $sth->bindParam("line", $line);
            $sth->execute();
            $days = $sth->fetchAll();

            $array_day = [];
            foreach ($days as &$d) {
                array_push($array_day, $d['id']);
            }
            $id_day = implode(',',$array_day);
        //}

        //echo '----->'.$id_day;

        // SELECT heure, SUM(total_appels) AS total_appels FROM `telephony_hours` GROUP BY `heure`
        // 

        $sql = 'SELECT heure, SUM(total_appels) AS total_appels, SUM(total_conversation) AS total_conversation, SUM(appels_ok) AS appels_ok, SUM(appels_ko) AS appels_ko, SUM(appels_membres) AS appels_membres, SUM(appels_externes) AS appels_externes FROM `telephony_hours` WHERE id_day IN ('.$id_day.') GROUP BY `heure` ORDER BY heure ASC';
        //echo $id_day;
        //echo $sql;
        $sth = $this->app->db->prepare($sql);
        //$sth->bindParam("id_day", $id_day);
        $sth->execute();
        $data = $sth->fetchAll();
        $stats['data'] = $data;
            
        return $this->app->response->withJson($stats)->withHeader('Access-Control-Allow-Origin', '*');
    }
}