<?php

namespace App\Controllers;

use \Firebase\JWT\JWT;
use \Mailjet\Resources;
use \DateTime;

$apikey = 'b5d50f14fd97f659f1ba203e511529b7';
$apisecret = '4590db7331da4db1724e7fb3d0652ed4';

setlocale(LC_ALL, "fr_FR");

class Common
{
    private $app;
    public function __construct($app)
    {
        $this->app = $app;
    }
    



    public function hello (){
        $settings = require __DIR__ . '/../../src/settings.php';
        print_r($settings['settings']['jwt']);
        $jwtsecret = 'ee';//$settings['jwt']['privateKey'];
        return $jwtsecret;
    }

    public function curl_get_contents($url){
      $ch = curl_init($url);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
      $data = curl_exec($ch);
      curl_close($ch);
      return $data;
    }

    // fonction de verification d'un droit
    public function checkRightsFromJwt ($request, $right) {
        $settings = require __DIR__ . '/../../src/settings.php';
        $jwtsecret = $settings['settings']['jwt']['privateKey'];
        $algorithm = $settings['settings']['jwt']['algorithm'];
        $jwt = Common::extractJwt($request);
        //echo $jwt;
        $decode =  JWT::decode($jwt, $jwtsecret, [$algorithm]);
        $rights = $decode->access;
        $valide = in_array($right, $rights);
        return $valide;
    }

    function getDatasFromJwt ($request, $key){
        $settings = require __DIR__ . '/../../src/settings.php';
        $jwtsecret = $settings['settings']['jwt']['privateKey'];
        $algorithm = $settings['settings']['jwt']['algorithm'];
        $jwt = Common::extractJwt($request);
        $data = JWT::decode($jwt, $jwtsecret, [$algorithm])->$key;
        return $data;
    }

    public function calculDiffTime ($start, $end) {
        $datetime1 = strtotime($start);
        $datetime2 = strtotime($end);
        $interval = $datetime2 - $datetime1; //date_diff($datetime1, $datetime2);
        //var_dump($datetime1);
        return $interval; //->format('%s');
    }

    // extraction du jwt de la request
    public function extractJwt ($request) {
        return explode(' ',$request->getHeaders()['HTTP_AUTHORIZATION'][0])[1];
    }

    // create d'un compte user
    function createAccount ($db, $data){
        $settings = require __DIR__ . '/../../src/settings.php';
        $jwtsecret = $settings['settings']['jwt']['privateKey'];
        $algorithm = $settings['settings']['jwt']['algorithm'];

        $encrypt = crypt('ualrt', $settings['settings']['jwt']['keyCryptPassword']); // mot de passe par defaut / clé

        $data['scope'] = "Customer";

        $sth = $db->prepare('INSERT INTO login (login, password, scope, verify) VALUES (:username,:password,:scope,0)');
        $sth->bindParam("username", $data['username']);
        $sth->bindParam("password", $encrypt);
        $sth->bindParam("scope", $data['scope']);
        $sth->execute();
        $loginId = $db->lastInsertId();
        
        $tokenData = array("username" => $data['username'], "scope" => $data['scope'], "id" => $data['id']);
            
        Common::sentMailVerificationLink($data,JWT::encode($tokenData, $jwtsecret, $algorithm ),true);
        
        $AND = "";
        if($data['trigramme'] == ''){
            $AND = ', trigramme = "U'.$data['id'].'"';
        }

        $sth = $db->prepare('UPDATE users SET id_login =:id_login '.$AND.' WHERE id =:id');
        $encrypt = crypt($input['password'], $input['username']);
        $sth->bindParam("id_login", $loginId);
        $sth->bindParam("id", $data['id']);
        $sth->execute();

        return array("statusCode" => 200, "code" => "Pending", "message" => "Merci de confirmer votre email en cliquant sur le lien du message");

    }

    function sentMailVerificationLink ($user, $token, $create) {
        $settings = require __DIR__ . '/../../src/settings.php';
        if($create){
            $mailbody = "<img src='".$settings['settings']['email']['url']."/img/email_header.png' /><br /><br /><p>Bonjour,<br>Un compte viens de vous être crée sur le planning des cours de l'UALRT.<br />Veuillez cliquer sur le lien ci-dessous pour confirmer votre accès<br /><br />mon identifiant : <b>".$user['username']."</b><br /><br /><a href='".$settings['settings']['email']['url']."/".$settings['settings']['email']['verifyEmailUrl']."?username=".$user['username']."&token=".$token."'>Je clique ici pour valider mon inscription</a></p><br /><br /> <img src='".$settings['settings']['email']['url']."/img/email_signature.png' />";
            $sujet = "Confirmation de votre compte";
        } else {
            $mailbody = "<img src='".$settings['settings']['email']['url']."/img/email_header.png' /><br /><br /><p>Bonjour,<br>Vous avez demandé à réinitialiser votre mot de passe<br />Veuillez cliquer sur le lien ci-dessous pour réinitialiser votre accès<br /><br />mon identifiant : <b>".$user['login']."</b><br /><br /><a href='".$settings['settings']['email']['url']."/".$settings['settings']['email']['verifyEmailUrl']."?username=".$user['login']."&token=".$token."'>je clique ici pour réinitialiser mon mot de passe</a></p><br /><br /> <img src='".$settings['settings']['email']['url']."/img/email_signature.png' />";
            $sujet = "Réinitialisation de votre mot de passe";
        }
        return Common::sendEmail($settings['settings']['email']['accountFrom'], $user['username'] , $sujet, $mailbody);

    }

    function sentMailAvailabilityAdded ($method, $data) {
        $settings = require __DIR__ . '/../../src/settings.php';
        $jwtsecret = $settings['settings']['jwt']['privateKey'];

        //var jour = new Date(data.date.date);
        $dateStart = new DateTime($data['input']['start']);
        $dateEnd = new DateTime($data['input']['end']);

        $jour = $dateStart->format('l d F Y');
        $start = $dateStart->format('H:i');
        $end = $dateEnd->format('H:i');
        
        $sujet = "";
        $message = "";
        $repeat = "";

        if($method == 'add'){
            $sujet = "Nouvelle";
            $message = "d'ajouter une nouvelle";
            $repeat = "(répétition ".$data['input']['nbRepeat']." fois)";
        } else if ($method == 'put'){
            $sujet = "Modification d'une";
            $message = "de modifier une";
            $repeat = "";
        } else if ($method == 'del'){
            $sujet = "Suppression d'une";
            $message = "de supprimer une ";
            $repeat = "(".$data['nbEvents']." évènement à replacer)";
        }
        
        $from = $settings['settings']['email']['accountName']." Planning<" .$settings['settings']['email']['accountFrom']. ">";
        $text = "Bonjour, <br/><b>".$data['user']->prenom." ".$data['user']->nom."</b> viens ".$message." disponibilité<br/><br/>";
        $text .= "Le <b>".$jour."</b> de <b>".$start."</b> à <b>".$end."</b> ".$repeat;
        //mail(from, Config.email.to , sujet+" dispo pour le planning de "+data.user.trigramme, mailbody);
        return Common::sendEmail ($from, $settings['settings']['email']['to'], $sujet." dispo pour le planning de ".$data['user']->trigramme, $text);
    }

    function sendEmail ($from, $to, $subject, $text) {
        $settings = require __DIR__ . '/../../src/settings.php';
        $jwtsecret = $settings['settings']['jwt']['privateKey'];
        $apikey = $settings['settings']['mailjet']['public'];
        $apisecret = $settings['settings']['mailjet']['private'];

        $mj = new \Mailjet\Client($apikey, $apisecret, true, ['version' => 'v3.1']);

        $body = [
            'Messages' => [
                [
                    'From' => [
                        'Email' => $from,
                        //'Name' => "UALRT Planning"
                    ],
                    'To' => [
                        [
                            'Email' => $to,
                            //'Name' => $user['prenom']." ".$user['nom']
                        ]
                    ],
                    'Subject' => $subject,
                    //'TextPart' => "Greetings from Mailjet!",
                    'HTMLPart' => $text
                ]
            ]
        ];

        $response = $mj->post(Resources::$Email, ['body' => $body]);
        $response->success();
        return $response->getData();
    }
}