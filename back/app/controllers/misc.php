<?php

namespace App\Controllers;

use \Firebase\JWT\JWT as JWT;
use \App\Controllers\Common as Common;
use \Ovh\Api;
use DateTime;

use Monolog\Logger;



class Misc 
{
    private $app;
    public function __construct($app)
    {
        $this->app = $app;
    }
    
    //////////////////////////////////////////////////////////////////////////
    // pour savoir si l'api est UP
    public function hello ($request, $response) {
        $composer = json_decode(file_get_contents(__DIR__ . '/../../composer.json'),true);
        $hello = array (
            "message" => "I'm ready !",
            "version" => $composer['version'],
            "lastUpdate" => $composer['lastUpdate'],
        );
        return $this->app->response->withJson($hello)->withHeader('Access-Control-Allow-Origin', '*');
    }

    //////////////////////////////////////////////////////////////////////////
    // recuperer l'identifié
    public function me ($request, $response) {
        $settings = $this->app->settings;
        $applicationKey = $settings['ovh']['applicationKey'];
        $applicationSecret = $settings['ovh']['applicationSecret'];
        $endpoint = $settings['ovh']['endpoint'];
        $consumerKey = $settings['ovh']['consumerKey'];

        $ovh = new Api( $applicationKey,
                $applicationSecret,
                $endpoint,
                $consumerKey);
        
        return $this->app->response->withJson($ovh->get('/me'))->withHeader('Access-Control-Allow-Origin', '*');
    }

    //////////////////////////////////////////////////////////////////////////
    // recuperer les creneau declarés de dispo du SVI
    public function conditionTime ($request, $response) {
        $settings = $this->app->settings;
        $applicationKey = $settings['ovh']['applicationKey'];
        $applicationSecret = $settings['ovh']['applicationSecret'];
        $endpoint = $settings['ovh']['endpoint'];
        $consumerKey = $settings['ovh']['consumerKey'];
        $billingAccount = $settings['ovh']['billingAccount'];
        $telNumber = $settings['ovh']['telNumber'];

        $date = $request->getQueryParam('date');

        $ovh = new Api( $applicationKey,
                $applicationSecret,
                $endpoint,
                $consumerKey);

                $array = [];

                // recuperation du dialplan
                $dialplan = $ovh->get('/telephony/'.$billingAccount.'/ovhPabx/'.$telNumber.'/dialplan');
                $extention = $ovh->get('/telephony/'.$billingAccount.'/ovhPabx/'.$telNumber.'/dialplan/'.$dialplan[0].'/extension');

                $conditionTime = $ovh->get('/telephony/'.$billingAccount.'/ovhPabx/'.$telNumber.'/dialplan/'.$dialplan[0].'/extension/'.$extention[0].'/conditionTime');
                foreach ($conditionTime as &$condition) {
                    $details = $ovh->get('/telephony/'.$billingAccount.'/ovhPabx/'.$telNumber.'/dialplan/'.$dialplan[0].'/extension/'.$extention[0].'/conditionTime/'.$condition);
                    unset($details['conditionId']);
                    array_push($array, $details);
                    $arrayFinal = $array;
                }

                if($date){

                    $weekDay = strtolower(date("l", strtotime($date)));

                    foreach($array as $key => $value){
                        if($value['weekDay'] != $weekDay){
                            unset($array[$key]);
                        }
                    }

                    $times = [];
                    // retraitement du array
                    foreach($array as $key => $value){
                        array_push($times, $value);
                        unset($value['weekDay']);
                    }

                    $arrayFinal = Array(
                        'date' => $date,
                        'weekDay' => $weekDay ,
                        'times' => $times   
                    );
                }
                
                return $this->app->response->withJson($arrayFinal)->withHeader('Access-Control-Allow-Origin', '*');
    }

    //////////////////////////////////////////////////////////////////////////
    // recuperer les lignes
    public function lines ($request, $response) {
        $settings = $this->app->settings;
        $applicationKey = $settings['ovh']['applicationKey'];
        $applicationSecret = $settings['ovh']['applicationSecret'];
        $endpoint = $settings['ovh']['endpoint'];
        $consumerKey = $settings['ovh']['consumerKey'];
        $billingAccount = $settings['ovh']['billingAccount'];
        $telNumber = $settings['ovh']['telNumber'];

        $ovh = new Api( $applicationKey,
                $applicationSecret,
                $endpoint,
                $consumerKey);

                $array = [];

                $listeLines = $ovh->get('/telephony/'.$billingAccount.'/ovhPabx/'.$telNumber.'/hunting/agent');
                foreach ($listeLines as &$line) {
                    $details = $ovh->get('/telephony/'.$billingAccount.'/ovhPabx/'.$telNumber.'/hunting/agent/'.$line);
                    array_push($array, $details);
                }
        
        
                $ligneSvi = $ovh->get('/telephony/'.$billingAccount.'/ovhPabx');
                foreach ($ligneSvi as &$ligne) {
                    $svi = $ovh->get('/telephony/'.$billingAccount.'/ovhPabx/'.$ligne);
                    array_push($array, array( 'number' => $svi['serviceName'], 'description' => $svi['description']));
                }
                
                return $this->app->response->withJson($array)->withHeader('Access-Control-Allow-Origin', '*');
    }

    //////////////////////////////////////////////////////////////////////////
    // les appels par lignes
    public function callsByLine ($request, $response, $args) {
        $settings = $this->app->settings;
        $applicationKey = $settings['ovh']['applicationKey'];
        $applicationSecret = $settings['ovh']['applicationSecret'];
        $endpoint = $settings['ovh']['endpoint'];
        $consumerKey = $settings['ovh']['consumerKey'];
        $billingAccount = $settings['ovh']['billingAccount'];
        $telNumber = $settings['ovh']['telNumber'];

        $from = NULL;
        $to = NULL;

        // pathParam
        $line = $args['line'];
        // QueryParam
        $from = $request->getQueryParam('from');// creationDatetime.from
        $to = $request->getQueryParam('to');// creationDatetime.to
        $display = $request->getQueryParam('display');

        $ovh = new Api( $applicationKey,
                $applicationSecret,
                $endpoint,
                $consumerKey);

        $appels = $ovh->get('/telephony/'.$billingAccount.'/service/'.$line.'/voiceConsumption', array(
            'creationDatetime.from' => $from, // Filter the value of creationDatetime property (>=) (type: datetime)
            'creationDatetime.to' => $to, // Filter the value of creationDatetime property (<=) (type: datetime)
            //'destinationType' => NULL, // Filter the value of destinationType property (=) (type: telephony.VoiceConsumptionDestinationTypeEnum)
            //'planType' => NULL, // Filter the value of planType property (=) (type: telephony.VoiceConsumptionPlanTypeEnum)
            //'wayType' => NULL, // Filter the value of wayType property (=) (type: telephony.VoiceConsumptionWayTypeEnum)
        ));

        $listeAppels = $appels;


        if($display === 'full'){
            $listeAppels = [];
            foreach ($appels as &$appel) {
                $details = $ovh->get('/telephony/'.$billingAccount.'/service/'.$line.'/voiceConsumption/'.$appel);
                array_push($listeAppels, $details);
            }
        }
          
        return $this->app->response->withJson($listeAppels)->withHeader('Access-Control-Allow-Origin', '*');
    }

    //////////////////////////////////////////////////////////////////////////
    // les appels par lignes
    public function callById ($request, $response, $args) {
        $settings = $this->app->settings;
        $applicationKey = $settings['ovh']['applicationKey'];
        $applicationSecret = $settings['ovh']['applicationSecret'];
        $endpoint = $settings['ovh']['endpoint'];
        $consumerKey = $settings['ovh']['consumerKey'];
        $billingAccount = $settings['ovh']['billingAccount'];
        $telNumber = $settings['ovh']['telNumber'];

        // pathParam
        $call = $args['call'];
        $line = $args['line'];

        $ovh = new Api( $applicationKey,
                $applicationSecret,
                $endpoint,
                $consumerKey);

        $appels = $ovh->get('/telephony/'.$billingAccount.'/service/'.$line.'/voiceConsumption/'.$call);
        
        
        return $this->app->response->withJson($appels)->withHeader('Access-Control-Allow-Origin', '*');
    }



    //////////////////////////////////////////////////////////////////////////
    // les messages de repondeur par lignes
    public function messagesByLine ($request, $response, $args) {
        $settings = $this->app->settings;
        $applicationKey = $settings['ovh']['applicationKey'];
        $applicationSecret = $settings['ovh']['applicationSecret'];
        $endpoint = $settings['ovh']['endpoint'];
        $consumerKey = $settings['ovh']['consumerKey'];
        $billingAccount = $settings['ovh']['billingAccount'];
        $telNumber = $settings['ovh']['telNumber'];

        $from = NULL;
        $to = NULL;

        // pathParam
        $line = $args['line'];
        // QueryParam
        $from = $request->getQueryParam('from');// creationDatetime.from
        $to = $request->getQueryParam('to');// creationDatetime.to
        $display = $request->getQueryParam('display');

        $ovh = new Api( $applicationKey,
                $applicationSecret,
                $endpoint,
                $consumerKey);
                $messages = $ovh->get('/telephony/'.$billingAccount.'/voicemail/'.$line.'/directories', array(
            //'creationDatetime.from' => $from, // Filter the value of creationDatetime property (>=) (type: datetime)
            //'creationDatetime.to' => $to, // Filter the value of creationDatetime property (<=) (type: datetime)
            //'destinationType' => NULL, // Filter the value of destinationType property (=) (type: telephony.VoiceConsumptionDestinationTypeEnum)
            //'planType' => NULL, // Filter the value of planType property (=) (type: telephony.VoiceConsumptionPlanTypeEnum)
            //'wayType' => NULL, // Filter the value of wayType property (=) (type: telephony.VoiceConsumptionWayTypeEnum)
        ));

        $listeMessages = $messages;


        if($display === 'full'){
            $listeMessages = [];

            $d_from = (new DateTime($from))->getTimestamp();
            $d_to = (new DateTime($to))->getTimestamp();

            foreach ($messages as &$message) {
                $details = $ovh->get('/telephony/'.$billingAccount.'/voicemail/'.$line.'/directories/'.$message);

                $ts_creationDatetime = (new DateTime($details['creationDatetime']))->getTimestamp();

                if($ts_creationDatetime >= $d_from && $ts_creationDatetime <= $d_to){
                    array_push($listeMessages, $details);
                }
            }
        }
          
        return $this->app->response->withJson($listeMessages)->withHeader('Access-Control-Allow-Origin', '*');
    }
}