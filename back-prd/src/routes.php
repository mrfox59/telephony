<?php
use \Firebase\JWT\JWT;

use \App\Controllers\Misc as misc;
use \App\Controllers\Stats as stats;

// healthcheck
$app->get('/hello ', misc::class . ':hello');

// users
$app->get('/me ', misc::class . ':me');
$app->get('/lines', misc::class . ':lines');
$app->get('/lines/{line}/calls', misc::class . ':callsByLine');
$app->get('/lines/{line}/calls/{call}', misc::class . ':callById');

// stats
$app->get('/stats/lines/{line}/date/{date}', stats::class . ':statsByDate');
$app->post('/stats/lines/{line}/date', stats::class . ':statsByMultiDate');

