<?php
return [
    'settings' => [
        'log.enabled' => true,
        'mode' => 'production',
        "debug" => true,
        'displayErrorDetails' => false, // set to false in production
        'addContentLengthHeader' => false, // Allow the web server to send the content-length header

        // Renderer settings
        'renderer' => [
            'template_path' => __DIR__ . '/../templates/',
        ],

        // Monolog settings
        'logger' => [
            'name' => 'slim-app',
            'path' => __DIR__ . '/../log/app.log',
            'level' => \Monolog\Logger::INFO,
        ],
        // Database connection settings           
        "db" => [
            "host" => "aeroclubzplannin.mysql.db",
            "dbname" => "aeroclubzplannin",
            "user" => "aeroclubzplannin",
            "pass" => "EA200300ualrt"
        ],
        "jwt" => [
            "privateKey" => '37LvDSm4XvjYOh9Y',
            "tokenExpiry" => 3600, // = 1h
            "algorithm" => "HS256",
            "keyCryptPassword" => "ualrt"
        ],
         "mailjet" => [ 
            "public" => "b5d50f14fd97f659f1ba203e511529b7",
            "private" => "4590db7331da4db1724e7fb3d0652ed4"
        ],
        "email" => [
            "accountFrom" => "secretariat@ualrt.org",
            "accountName"=> "UALRT",
            "verifyEmailUrl"=> "confirm.html",
            "url"=> "https://planning.ualrt.org",
            "to"=> "secretariat@ualrt.org"
        ]   
    ],
];
