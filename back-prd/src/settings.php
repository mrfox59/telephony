<?php
return [
    'settings' => [
        'log.enabled' => true,
        'mode' => 'development',
        "debug" => true,
        'displayErrorDetails' => true, // set to false in production
        'addContentLengthHeader' => false, // Allow the web server to send the content-length header

        // Renderer settings
        'renderer' => [
            'template_path' => __DIR__ . '/../templates/',
        ],

        // Monolog settings
        'logger' => [
            'name' => 'slim-app',
            'path' => __DIR__ . '/../log/app.log',
            'level' => \Monolog\Logger::INFO,
        ],
        // Database connection settings           
        "db" => [
            "host" => "aeroclubzplannin.mysql.db",
            "dbname" => "aeroclubzplannin",
            "user" => "aeroclubzplannin",
            "pass" => "EA200300ualrt"
        ],
        "ovh" => [
            "applicationKey" => "k4pG9UxgvCKGxRpn",
            "applicationSecret" => "Ra3oMYJrrYkLtebIzFog1ShOafUGnA0Q",
            "consumerKey" => "A4GRixO8CF7Pa67odwdrovpD775ueXUW",
            "endpoint" => "ovh-eu",
            "billingAccount" => "pg49279-ovh-1",
            "telNumber" => "0033972218570",
            "redirection" => "zzzzz"
        ],
        "jwt" => [
            "privateKey" => '37LvDSm4XvjYOh9Y',
            "tokenExpiry" => 3600, // = 3600s = 1h
            "algorithm" => "HS256",
            "keyCryptPassword" => "ualrt"
        ] 
    ],
];
