<?php

namespace App\Middlewares;
 
class AppMiddleware
{
    public function __construct($settings)
    {
        $this->settings = $settings;
    }
 
    public function run($request, $response, $next)
    {
        $settings = $this->settings;
        // do something with $settings
        return $next($request, $response, $next);
    }
}