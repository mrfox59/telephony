<?php

namespace App\Controllers;

use \Firebase\JWT\JWT as JWT;
use \App\Controllers\Common as Common;
use \Ovh\Api;


class Misc 
{
    private $app;
    public function __construct($app)
    {
        $this->app = $app;
    }
    
    //////////////////////////////////////////////////////////////////////////
    // pour savoir si l'api est UP
    public function hello ($request, $response) {
        $composer = json_decode(file_get_contents(__DIR__ . '/../../composer.json'),true);
        $hello = array (
            "message" => "I'm ready !",
            "version" => $composer['version'],
            "lastUpdate" => $composer['lastUpdate'],
        );
        return $this->app->response->withJson($hello)->withHeader('Access-Control-Allow-Origin', '*');
    }

    //////////////////////////////////////////////////////////////////////////
    // recuperer l'identifié
    public function me ($request, $response) {
        $settings = $this->app->settings;
        $applicationKey = $settings['ovh']['applicationKey'];
        $applicationSecret = $settings['ovh']['applicationSecret'];
        $endpoint = $settings['ovh']['endpoint'];
        $consumerKey = $settings['ovh']['consumerKey'];

        $ovh = new Api( $applicationKey,
                $applicationSecret,
                $endpoint,
                $consumerKey);
        
        return $this->app->response->withJson($ovh->get('/me'))->withHeader('Access-Control-Allow-Origin', '*');
    }

    //////////////////////////////////////////////////////////////////////////
    // recuperer les lignes
    public function lines ($request, $response) {
        $settings = $this->app->settings;
        $applicationKey = $settings['ovh']['applicationKey'];
        $applicationSecret = $settings['ovh']['applicationSecret'];
        $endpoint = $settings['ovh']['endpoint'];
        $consumerKey = $settings['ovh']['consumerKey'];
        $billingAccount = $settings['ovh']['billingAccount'];
        $telNumber = $settings['ovh']['telNumber'];

        $ovh = new Api( $applicationKey,
                $applicationSecret,
                $endpoint,
                $consumerKey);

                $array = [];

                $listeLines = $ovh->get('/telephony/'.$billingAccount.'/ovhPabx/'.$telNumber.'/hunting/agent');
                foreach ($listeLines as &$line) {
                    $details = $ovh->get('/telephony/'.$billingAccount.'/ovhPabx/'.$telNumber.'/hunting/agent/'.$line);
                    array_push($array, $details);
                }
        
        
                $ligneSvi = $ovh->get('/telephony/'.$billingAccount.'/ovhPabx');
                foreach ($ligneSvi as &$ligne) {
                    $svi = $ovh->get('/telephony/'.$billingAccount.'/ovhPabx/'.$ligne);
                    array_push($array, array( 'number' => $svi['serviceName'], 'description' => $svi['description']));
                }
                
                return $this->app->response->withJson($array)->withHeader('Access-Control-Allow-Origin', '*');
    }

    //////////////////////////////////////////////////////////////////////////
    // les appels par lignes
    public function callsByLine ($request, $response, $args) {
        $settings = $this->app->settings;
        $applicationKey = $settings['ovh']['applicationKey'];
        $applicationSecret = $settings['ovh']['applicationSecret'];
        $endpoint = $settings['ovh']['endpoint'];
        $consumerKey = $settings['ovh']['consumerKey'];
        $billingAccount = $settings['ovh']['billingAccount'];
        $telNumber = $settings['ovh']['telNumber'];

        $from = NULL;
        $to = NULL;

        // pathParam
        $line = $args['line'];
        // QueryParam
        $from = $request->getQueryParam('from');// creationDatetime.from
        $to = $request->getQueryParam('to');// creationDatetime.to
        $display = $request->getQueryParam('display');

        $ovh = new Api( $applicationKey,
                $applicationSecret,
                $endpoint,
                $consumerKey);

        $appels = $ovh->get('/telephony/'.$billingAccount.'/service/'.$line.'/voiceConsumption', array(
            'creationDatetime.from' => $from, // Filter the value of creationDatetime property (>=) (type: datetime)
            'creationDatetime.to' => $to, // Filter the value of creationDatetime property (<=) (type: datetime)
            //'destinationType' => NULL, // Filter the value of destinationType property (=) (type: telephony.VoiceConsumptionDestinationTypeEnum)
            //'planType' => NULL, // Filter the value of planType property (=) (type: telephony.VoiceConsumptionPlanTypeEnum)
            //'wayType' => NULL, // Filter the value of wayType property (=) (type: telephony.VoiceConsumptionWayTypeEnum)
        ));

        $listeAppels = $appels;


        if($display === 'full'){
            $listeAppels = [];
            foreach ($appels as &$appel) {
                $details = $ovh->get('/telephony/'.$billingAccount.'/service/'.$line.'/voiceConsumption/'.$appel);
                array_push($listeAppels, $details);
            }
        }
          
        return $this->app->response->withJson($listeAppels)->withHeader('Access-Control-Allow-Origin', '*');
    }

    //////////////////////////////////////////////////////////////////////////
    // les appels par lignes
    public function callById ($request, $response, $args) {
        $settings = $this->app->settings;
        $applicationKey = $settings['ovh']['applicationKey'];
        $applicationSecret = $settings['ovh']['applicationSecret'];
        $endpoint = $settings['ovh']['endpoint'];
        $consumerKey = $settings['ovh']['consumerKey'];
        $billingAccount = $settings['ovh']['billingAccount'];
        $telNumber = $settings['ovh']['telNumber'];

        // pathParam
        $call = $args['call'];
        $line = $args['line'];

        $ovh = new Api( $applicationKey,
                $applicationSecret,
                $endpoint,
                $consumerKey);

        $appels = $ovh->get('/telephony/'.$billingAccount.'/service/'.$line.'/voiceConsumption/'.$call);
        
        
        return $this->app->response->withJson($appels)->withHeader('Access-Control-Allow-Origin', '*');
    }
}