<?php

namespace App\Controllers;

use \Firebase\JWT\JWT as JWT;
use \App\Controllers\Common as Common;
use DateTime;


class Stats 
{
    private $app;
    public function __construct($app)
    {
        $this->app = $app;
    }

    //////////////////////////////////////////////////////////////////////////
    // les appels par lignes
    public function statsByDate ($request, $response, $args) {
        $settings = $this->app->settings;
        // pathParam
        $date = $args['date']; // au format YYYY-MM-DD
        $line = $args['line'];

        $stats = [];

        if(strlen($date) == 10){ // YYYY-MM-DD
            $sql = 'SELECT * FROM `telephony_day` WHERE `numero` = :line AND `date` LIKE :date';
            $id_day = $day->id;
        } else { // YYYY-MM ou YYYY
            $sql = 'SELECT SUM(total_svi) AS total_svi, SUM(total_appels) AS total_appels, SUM(total_conversation) AS total_conversation, SUM(appels_ok) AS appels_ok, SUM(appels_ko) AS appels_ko, SUM(appels_membres) AS appels_membres, SUM(appels_externes) AS appels_externes, MIN(heure_first_call) AS heure_first_call, MAX(heure_last_call) AS heure_last_call FROM `telephony_day` WHERE `numero` = :line AND `date` LIKE :date AND `total_appels` != "00:00"';
        }
        
        $sth = $this->app->db->prepare($sql);
        $sth->bindValue("date", '%'.$date.'%');
        $sth->bindParam("line", $line);
        $sth->execute();
        $day = $sth->fetchAll();

        $stats['day'] = $day[0];
        $stats['day']['number'] = $line;

        if(strlen($date) == 10){ // YYYY-MM-DD
            
            $id_day = $day[0]['id'];
        } else { // YYYY-MM ou YYYY
            $sth = $this->app->db->prepare('SELECT id FROM `telephony_day` WHERE `numero` = :line AND `date` LIKE :date AND `total_appels` != "00:00"');
            $sth->bindValue("date", '%'.$date.'%');
            $sth->bindParam("line", $line);
            $sth->execute();
            $days = $sth->fetchAll();

            $array_day = [];
            foreach ($days as &$d) {
                array_push($array_day, $d['id']);
            }
            $id_day = implode(',',$array_day);
        }

        //echo '----->'.$id_day;

        // SELECT heure, SUM(total_appels) AS total_appels FROM `telephony_hours` GROUP BY `heure`
        // 

        $sql = 'SELECT heure, SUM(total_appels) AS total_appels, SUM(total_conversation) AS total_conversation, SUM(appels_ok) AS appels_ok, SUM(appels_ko) AS appels_ko, SUM(appels_membres) AS appels_membres, SUM(appels_externes) AS appels_externes FROM `telephony_hours` WHERE id_day IN ('.$id_day.') GROUP BY `heure` ORDER BY heure ASC';
        //echo $id_day;
        //echo $sql;
        $sth = $this->app->db->prepare($sql);
        //$sth->bindParam("id_day", $id_day);
        $sth->execute();
        $data = $sth->fetchAll();
        $stats['data'] = $data;
            
        return $this->app->response->withJson($stats)->withHeader('Access-Control-Allow-Origin', '*');
    }

    //////////////////////////////////////////////////////////////////////////
    // les appels par lignes en fonction d'un array de date
    public function statsByMultiDate ($request, $response, $args) {
        $settings = $this->app->settings;
        // pathParam
        $line = $args['line'];

        // parsing du body
        $input = $request->getParsedBody();
        $date = str_replace(' ', '',$input['dates']);

        $array_dates = explode(",", $date);

        $stats = [];

        $dates_in = '"'.join("\",\"",$array_dates).'"';
        echo $dates_in;

        // if(strlen($date) == 10){ // YYYY-MM-DD
        //     $sql = 'SELECT * FROM `telephony_day` WHERE `numero` = :line AND `date` LIKE :date';
        //     $id_day = $day->id;
        // } else { // YYYY-MM ou YYYY
            $sql = 'SELECT SUM(total_svi) AS total_svi, SUM(total_appels) AS total_appels, SUM(total_conversation) AS total_conversation, SUM(appels_ok) AS appels_ok, SUM(appels_ko) AS appels_ko, SUM(appels_membres) AS appels_membres, SUM(appels_externes) AS appels_externes, MIN(heure_first_call) AS heure_first_call, MAX(heure_last_call) AS heure_last_call FROM `telephony_day` WHERE `numero` = :line AND `date` IN ('.$dates_in.') AND `total_appels` != "00:00"';
        //}
        
        $sth = $this->app->db->prepare($sql);
        $sth->bindParam("line", $line);
        $sth->execute();
        $day = $sth->fetchAll();

        $stats['day'] = $day[0];
        $stats['day']['number'] = $line;

        //print_r($stats);

        // if(strlen($date) == 10){ // YYYY-MM-DD
            
        //     $id_day = $day[0]['id'];
        // } else { // YYYY-MM ou YYYY
            $sth = $this->app->db->prepare('SELECT id FROM `telephony_day` WHERE `numero` = :line AND `date`  IN ('.$dates_in.') AND `total_appels` != "00:00"');
            // $sth->bindValue("date", '%'.$date.'%');
            $sth->bindParam("line", $line);
            $sth->execute();
            $days = $sth->fetchAll();

            $array_day = [];
            foreach ($days as &$d) {
                array_push($array_day, $d['id']);
            }
            $id_day = implode(',',$array_day);
        //}

        //echo '----->'.$id_day;

        // SELECT heure, SUM(total_appels) AS total_appels FROM `telephony_hours` GROUP BY `heure`
        // 

        $sql = 'SELECT heure, SUM(total_appels) AS total_appels, SUM(total_conversation) AS total_conversation, SUM(appels_ok) AS appels_ok, SUM(appels_ko) AS appels_ko, SUM(appels_membres) AS appels_membres, SUM(appels_externes) AS appels_externes FROM `telephony_hours` WHERE id_day IN ('.$id_day.') GROUP BY `heure` ORDER BY heure ASC';
        //echo $id_day;
        //echo $sql;
        $sth = $this->app->db->prepare($sql);
        //$sth->bindParam("id_day", $id_day);
        $sth->execute();
        $data = $sth->fetchAll();
        $stats['data'] = $data;
            
        return $this->app->response->withJson($stats)->withHeader('Access-Control-Allow-Origin', '*');
    }
}