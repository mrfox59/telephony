import * as env from"./env.js";
$(function() {
    const url = env.baseUrl;

    // ajout de la ligne des boutons de sondes
    $('#content').append('<div class="navbar" id="lines"></div>');
    $('#content').append('<div id="datas"><h3></h3><pre></pre></div>');
    

    let listeCalls = [];

    $("#from").val(moment().format("YYYY/MM/DD")+" 00:00");
    $("#to").val(moment().format("YYYY/MM/DD")+" 23:59");

    $('#from, #to').datetimepicker({
        inline:false,
        format:'Y/m/d H:i'
    });


    //function refreshStatus(){
        //$("#date").html(moment().format('HH:mm'));
        $.ajax({
            url : url+'/lines',
            type : 'GET',
            statusCode: {
                200: function (res) {
                    var btn = '';
                    $.each(res, function (index_sondes, value) {
                        $('#content #lines').html('');
                        var color = 'danger';
                        if(value.status == 'available'){
                            color = 'success';
                        }
						btn += '<button type="button" class="btn btn-'+color+' btn-md" data_number="'+value.number+'"><i class="fas fa-phone"></i>&nbsp;'+value.description+'</button>&nbsp;';
                    })
                    $('#content #lines').append(btn);
                }
            },
            error: function (request, status, error) {
                console.log('error');
            }
        });
    //}

    //
    function getDetails (datas) {
        $('#content #datas').html('');
        $.ajax({
            url : url+'/lines/'+datas['number']+'/calls',
            data: {"from": datas['datetimeFrom'],"to":datas['datetimeTo']},
            type : 'GET',
            statusCode: {
                200: function (res) {
                    listeCalls = res;
                    console.log(res.length + ' appels entre '+datas['from']+ ' et '+datas['to']);
                    $('#content #datas').html('<h3>'+res.length + ' appels entre '+datas['from']+ ' et '+datas['to']+'</h3>');
                    $('#content #datas').append('<button type="button" class="btn btn-secondary btn-sm" id="more">Voir en details</button>');
                }
            },
            error: function (request, status, error) {
                console.log('error');
            }
        });
    }

    //



    $( "#content #lines" ).delegate( "button", "click", function() {
        var number = $(this).attr('data_number');
        var from = $("#from").val().replace(/\//gi,'-').replace(/ /gi,'T');
        var to = $("#to").val().replace(/\//gi,'-').replace(/ /gi,'T');
        //var nom = $(this).text();
        console.log('clic sur '+number+" entre "+from+" et "+to);
        datas['number'] = number;
        datas['datetimeFrom'] = from;
        datas['datetimeTo'] = to;
        datas['from'] = $("#from").val();
        datas['to'] = $("#to").val();
        getDetails(datas);
        //$('#content #datas h3').text('').append(nom+'<span></span>');
    });

    $( "#content #datas" ).delegate( "button", "click", function() {
        console.log('clic more');
        $('#content #datas').append(`<table class="table">
            <thead>
            <tr>
                <th scope="col">Date</th>
                <th scope="col">Appelant</th>
                <th scope="col">Durée</th>
            </tr>
            </thead>
            <tbody></tbody>
            </table>`);

        
        $.ajax({
            url : url+'/lines/'+datas['number']+'/calls',
            data: {"from": datas['datetimeFrom'],"to":datas['datetimeTo'], "display":"full"},
            type : 'GET',
            statusCode: {
                200: function (res) {
                    $.each(res, function (res, v) {
                    $('#content #datas .table tbody').append('<tr><th scope="row">'+moment(v.creationDatetime).format('DD MMM YYYY \à HH:mm:ss')+'</th><td>'+v.calling+'</td><td>'+v.duration+' sec.</td></tr>');
                    });
                }
            },
            error: function (request, status, error) {
                console.log('error');
            }
        });    

    });

});