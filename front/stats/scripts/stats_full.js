import * as env from"./env.js";
$(function() {
    const url = env.baseUrl;

    function init_content (){
        $('#content').html('');
        $('#content').append('<div id="stats" class="row mt-4"></div>');
        $('#content').append('<div id="bargraph_horaire" class="row mt-2"></div>');
        $('#content').append('<div id="conversation" class="row mt-2"></div>');
    }

    init_content();

    function n(n){
        return n > 9 ? "" + n: "0" + n;
    }

    // hide le button le temps du chargement
    //$('#interval button').hide();
    $('select').attr('disabled','disabled');
    $('button').attr('disabled','disabled');

    // generation de la liste des lignes
    $.ajax({
        url : url+'/lines',
        type : 'GET',
        statusCode: {
            200: function (res) {
                $.each(res, function (index, value) {
                    if(value.agentId){
                        $('#lines').append('<option value="'+value.number+'">'+value.description+'</option>');
                    }
                });
                $('select').attr('disabled',false);
                $('button').attr('disabled',false);
            }
        },
        error: function (request, status, error) {
            console.log('error');
        }
    });

    var hier = new Date(moment().subtract(1, 'd'));
    
    // generatiopn de la liste des jours
    var jour = hier.getDate();
    for(var i=1; i<32; i++){
        if(jour == i){
            $('#jour').append('<option value="-'+n(i)+'" selected="selected">'+n(i)+'</option>');
        } else {
            $('#jour').append('<option value="-'+n(i)+'">'+n(i)+'</option>');
        }
    }

    // generation de la liste des mois
    var months = ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Aout', 'Septembre', 'Octobre', 'Novembre', 'Décembre'];
    var mois = hier.getMonth();
    for(var i=0; i<12; i++){
        if(mois == i){
            $('#mois').append('<option value="-'+n(i+1)+'" selected="selected">'+months[i]+'</option>');
        }else {
            $('#mois').append('<option value="-'+n(i+1)+'">'+months[i]+'</option>');
        }
    }

    // generation de la liste des années
    var year = new Date().getFullYear();
    for(var i=2019; i <= year; i++){
        $('#annee').append('<option value="'+i+'">'+i+'</option>');
    }

    // function generation du rapport
    function generateRapport(){
        init_content();
        var jour = $( "#jour option:selected" ).val();
        var mois = $( "#mois option:selected" ).val();
        var annee = $( "#annee option:selected" ).val();
        var line = $( "#lines option:selected" ).val();
        var date = annee+mois+jour;

        var format = 'dddd DD MMMM YYYY';
        var article = ' du ';
        if(jour == 0){
            format = 'MMMM YYYY';
            article = ' du mois de ';
        }
        if (mois == 0){
            format = 'YYYY';
            article = ' de l’année ';
        }

        var day = moment(annee+''+mois+''+jour).format(format);

        var text = 'Compte-rendu '+article+''+day;

        $('#date_full_header').text(text);

        var url_api = `${url}/stats/lines/${line}/date/${date}`;
        console.log(url_api);

        // on get les valeur via l'api
        $.ajax({
            url : url_api,
            type : 'GET',
            statusCode: {
                200: function (res) {
                    taux_de_la_ligne(res.day);
                    details_appels(res.day);
                    taux_de_reponse(res.day);
                    bargraph_appels_horaire(res.data);
                    conversation_moyenne(res.day);
                    bargraph_conversation_horaire(res.data);
                    console.log();
                    $.each(res.data, function (index, value) {
                        //console.log(value)
                    });
                    $('#interval button').show();
                }
            },
            error: function (request, status, error) {
                console.log('error');
            }
        });
    }

    // creation et affichage d'un graph
    function charts(datas){
        var details = datas.details;
        var dates = datas.dates;
        var series = datas.series;

        console.log(datas);


    	Highcharts.chart(details.graph, {
		    chart: {
                renderTo: details.graph,
                type: 'column',
                width: details.width,
                height:details.height,
		        scrollablePlotArea: {
		            scrollPositionX: 1
                }
		    },
		    title: {
		        text: ' ',
		        align: 'left'
			},
			xAxis: {
				categories: dates,
					
		        labels: {
		            overflow: 'justify'
		        }
		    },
		    yAxis: {
		        title: {
		            text: details.nom
                },
                visible: false,
		        minorGridLineWidth: 0,
		        gridLineWidth: 0,
		        alternateGridColor: null,
		    },
		    tooltip: {
                formatter: function () {
                    return '<b>' + this.x + '</b><br/>' +
                        this.series.name + ': ' + conversion_secondes(this.y) + '<br/>';
                        //'Total: ' + conversion_secondes(this.point.stackTotal);
                }
            },
            plotOptions: {
                column: {
                    stacking: 'normal'
                }
            },
		    series: series
		});
    }

    // percentage
    function percentage(value, total){
        return Math.round((value/total)*100);
    }

    // Bargraph des appels par tranches horaire
    function bargraph_appels_horaire (data){

        let appels_membres = [];
        let appels_ko = [];
        let appels_ok = [];
        let appels_externes = [];
        let dates = [];
        //console.log(obj.solde); 
        $.each(data, function(i, el) {
            appels_membres.push(Number(el.appels_membres));
            appels_externes.push(Number(el.appels_externes));
            appels_ko.push(Number(el.appels_ko));
            appels_ok.push(Number(el.appels_ok));
            dates.push('de '+el.heure+'h à '+(Number(el.heure)+1)+'h');
        });

        // construction des bars
        var series = [
            {
                name: 'Appels des membres',
                data: appels_membres,
                stack: 'type',
                color: '#EBD08A'
            },
            {
                name: 'Appels des externes',
                data: appels_externes,
                stack: 'type',
                color: '#C09CEB'
            },
            {
                name: 'Appels ratés',
                data: appels_ko,
                stack: 'appels',
                color: '#D15F5E'
            },
            {
                name: 'Appels pris',
                data: appels_ok,
                stack: 'appels',
                color: '#9DEB91'
            }
        ];

        var datas = {
            'details' : {
                'graph' : 'bargraph_appels_horaire',
                'width' : null,
                'height' : 350
            },
            'dates' : dates,
            'series' : series
        };
        
        // création du html
        var html = `
        <div class="col border rounded mx-1 px-2">
            <h4 class="text-secondary text-center">Nombres d'appels par heure</h4>
            <div id="bargraph_appels_horaire"></div>
        </div>
        `;

        $('#content #bargraph_horaire').append(html);
        charts(datas);
    }

    // Bargraph des durée de conversation par tranches horaire
    function bargraph_conversation_horaire (data){

        let values = [];
        let dates = [];
        //console.log(obj.solde); 
        $.each(data, function(i, el) {
            values.push(Number(el.total_conversation));
            dates.push('de '+el.heure+'h à '+(Number(el.heure)+1)+'h');
        });

        var series = [
            {
                name: 'Temps en conversation',
                data: values,
                stack: 'type',
                color: '#EBBF49'
            }
        ]

        var datas = {
            'details' : {
                'graph' : 'bargraph_conversation_horaire',
                'width' : 540,
                'height' : 250
            },
            'dates' : dates,
            'series' : series
        };
        
        // création du html
        var html = `
        <div class="col border rounded mx-1 px-2">
            <h4 class="text-secondary text-center">Durée des conversations par heure</h4>
            <div id="bargraph_conversation_horaire"></div>
        </div>
        `;

        $('#content #conversation').append(html);
        charts(datas);
    }

    // calcul du taux de transfert vers la ligne
    function taux_de_la_ligne (data){
        var taux_de_la_ligne = percentage(data.total_appels, data.total_svi);
        
        // création du html
        var html = `
        <div class="col border rounded mx-1 px-2">
            <h4 class="text-secondary text-center">Taux de transfert</h4>
            <h1 class="text-center font-weight-bold">${taux_de_la_ligne}%</h1>
            <p class="text-secondary">
            Nombre d'appels sur le SVI : <span class="font-weight-bold">${data.total_svi}</span><br/>
            Nombre de transfert vers la ligne : <span class="font-weight-bold">${data.total_appels}</span>
            </p>
        </div>
        `;

        $('#content #stats').append(html);
    }

    // calcul du taux de reponse
    function taux_de_reponse (data){
        var taux_de_reponse = percentage(data.appels_ok, data.total_appels);
        
        // création du html
        var html = `
        <div class="col border rounded mx-1 px-2">
            <h4 class="text-secondary text-center">Taux de réponse</h4>
            <h1 class="text-center font-weight-bold">${taux_de_reponse}%</h1>
            <p class="text-secondary">
            Nombre d'appels entrants : <span class="font-weight-bold">${data.total_appels}</span><br/>
            Nombre d'appels répondus : <span class="font-weight-bold">${data.appels_ok}</span>
            </p>
        </div>
        `;

        $('#content #stats').append(html);
    }

    // Details des données
    function details_appels (data){
        var pct_appels_membres = percentage(data.appels_membres, data.total_appels);
        var pct_appels_externes = percentage(data.appels_externes, data.total_appels);
        
        // création du html
        var html = `
        <div class="col border rounded mx-1 px-2">
            <h4 class="text-secondary text-center">Détails des appels</h4>
            <h2 class="text-center"><span class="font-weight-bold">${data.total_appels}</span> appels entrants</h2>
            <p class="text-secondary">
            dont appels de membres : <span class="font-weight-bold">${data.appels_membres}</span> soit ${pct_appels_membres}%<br/>
            dont appels d'externes : <span class="font-weight-bold">${data.appels_externes}</span> soit ${pct_appels_externes}%<br/>
            <small>Premier appel à <span class="font-weight-bold">${data.heure_first_call}</span>, dernier appel à <span class="font-weight-bold">${data.heure_last_call}</span></small>
            </p>
        </div>
        `;

        $('#content #stats').append(html);
    }

    // calcul du temps moyen de communication
    function conversation_moyenne (data){
        var conversation_moyenne = conversion_secondes(Math.round(data.total_conversation / data.appels_ok));
        var conversation_cumulee = conversion_secondes(Number(data.total_conversation));
        
        // création du html
        var html = `
        <div class="col border rounded mx-1 px-2">
            <h4 class="text-secondary text-center">Moyenne en communication</h4>
            <h1 class="text-center">${conversation_moyenne}</h1>
            <p class="text-secondary">
            temps d'appel cumulé : <span class="font-weight-bold">${conversation_cumulee}</span><br/>
            Nombre d'appels repondu : <span class="font-weight-bold">${data.appels_ok}</span>
            </p>
        </div>
        `;

        $('#content #conversation').append(html);
    }

    function conversion_secondes (secondes){
        var duration = moment.duration(secondes, 'seconds');
        if(secondes >= 3600){
            return duration.format("hh:mm\'ss");
        } else {
            return duration.format("mm\'ss");
        }
    }

    $( "#interval" ).delegate( "button", "click", function() {
        generateRapport();
    });

});