import * as env from"./env.js";
$(function() {
    const url = env.baseUrl;
    //let listeCalls = [];
    let listeCallsId = [];
    let listeOuvertureBureau = [];
    //let listeCallsAndMessages = [];
    var datas = [];

    // ajout de la ligne des boutons de sondes
    $('#content').append('<div class="navbar" id="lines"></div>');
    $('#content').append('<div id="datas"><h3></h3><h2></h2><pre></pre></div>');


    $('#loading').hide();
    

    $("#from").val(moment().format("YYYY/MM/DD")+" 00:00");
    $("#to").val(moment().format("YYYY/MM/DD")+" 23:59");

    $('#from, #to').datetimepicker({
        inline:false,
        format:'Y/m/d H:i'
    });

                

    // foncrtion qui lance la generation des stats
    // $( "#interval" ).delegate( "#show", "click", function() {
    //     generateDetails();
    // });

    function getCallsAndMessages () {

        var from = $("#from").val().replace(/\//gi,'-').replace(/ /gi,'T');
        var to = $("#to").val().replace(/\//gi,'-').replace(/ /gi,'T');

        datas['datetimeFrom'] = from;
        datas['datetimeTo'] = to;
        datas['from'] = $("#from").val();
        datas['to'] = $("#to").val();
        datas['display'] = 'full';

        var promises_lines = [];

        $.ajax({
            url : url+'/lines',
            type : 'GET',
            statusCode: {
                200: function (res) {
                    //console.log( datas['from']);
                    $.each(res, function (index, value) {
                        if(value.timeout){
                            datas['number'] = value.number;
                            promises_lines.push(getCalls(datas));
                        }
                    });

                    $.when.apply($,promises_lines).then(function() {
                        var objects=arguments;
                        listeCallsId = [];
                        //console.log(objects);
                        //var total = 0;
                        //console.log(objects);
                        $.each(objects, function (i, v) {
                            $.each(v[0], function (i, c) {
                                //console.log(c);
                                listeCallsId.push( moment(c.creationDatetime).format('YYYYMMDDHHmmss')+'-'+c.dialed+'-'+c.calling ) ;// = listeCalls.concat(v[0])
                            });
                        })
                        //listeCalls.sort();
                        console.log('listeCallsId',listeCallsId);
                    });
                }
            },
            error: function (request, status, error) {
                console.log('error');
            }
        });


        

        $('#loading').show();
        $('#content #datas .table').html('');
        $('#content #datas #jumbotron').html('');
        var promises = [];

        
        //var nom = $(this).text();
        //console.log('clic sur '+number+" entre "+from+" et "+to);

        datas['number'] = '0033972218570'
        promises.push({'calls' : (getCalls(datas))});
        datas['number'] = '0033374096724'
        promises.push({'messages' : (getMessages(datas))});

        $.when.apply($,promises).then(function() {
            var objects=arguments;
            //var listeCallsAndMessages = [];
            //var total = 0;
            objects[0]['calls'].then(function(calls) {
                objects[1]['messages'].then(function(messages) {
                    // ici tous les id des elements
                    //console.log(messages);
                    //console.log(calls);
                    // traitement des listes
                    var liste_full = [];
                    // les messages
                    $.each(messages, function (i, m) {
                        var message = {};
                        message['creationDatetime'] = m.creationDatetime;
                        message['type'] = 'message';
                        message['num_from'] = m.caller;
                        message['num_to'] = m.callee
                        message['duration'] = m.duration;
                        liste_full.push(message);
                    });
                    // les appels
                    $.each(calls, function (i, c) {
                        var call = {};
                        call['creationDatetime'] = c.creationDatetime;
                        call['type'] = 'call';
                        call['num_from'] = c.calling;
                        call['num_to'] = c.called
                        call['duration'] = c.duration;
                    
                        liste_full.push(call);
                    });

                    // classement ASC des appels + messages
                    liste_full.sort(function (a, b) {
                        return new Date(a.creationDatetime).getTime() - new Date(b.creationDatetime).getTime();
                      }
                    );

                    console.log('liste_full',liste_full);

                    

                    // fonction de traitement globale
                    traitement(liste_full);

                });
            });

        });
    }

    // function de traitement des appels recus
    function traitement (listeCalls){

        $('#loading').hide();

        $('#content #datas').html(`<div id="jumbotron">
      </div>
        <table class="table">
        <thead>
        <tr>
            <th scope="col"></th>
            <th scope="col">Date</th>
            <th scope="col">Heure</th>
            <th scope="col">Appelant</th>
            <th scope="col">Durée (s)</th>
            <th scope="col">Vers</th>
            <th scope="col">Etat</th>
            <th scope="col">state</th>
            <th scope="col">attente (s)</th>
            <th scope="col">final</th>
        </tr>
        </thead>
        <tbody></tbody>
        </table>`);


        var listeCallsCustom = [];
        var tel1;
        var tel2;
        var duration1;
        var duration2;
        var idLinePrevious = 0;
        var loop = 0;
        var time = 0;
        var wait = 0;
        $.each(listeCalls, function (i, v) {
            var vCustom = v;
            vCustom['id'] = moment(v.creationDatetime).format('YYYYMMDDHHmmss')+'-'+v.num_to+'-'+v.num_from;
            //if(listeCallsId.includes(vCustom.id)){
                vCustom['include'] = listeCallsId.includes(vCustom.id);
            //}
            if(v.type == 'message'){
                vCustom['final'] = 'repondeur';
                vCustom['bg'] = 'table-warning';
                vCustom['nbLoop'] = '';
                vCustom['newCall'] = 'no';
                vCustom['state'] = 'message';
                vCustom['hangupAfterSeconds'] = '';
            }
            else if(v.type == 'call'){
                //var d = {};
                //d['etat'] = v.hangupNature;
                //var moreTd = '<td>start</td>'
                vCustom['state'] = 'start';
                vCustom['nbLoop'] = 0;
                vCustom['hangupAfterSeconds'] = 0;
                vCustom['bg'] = '';
                var newCall = true;
                vCustom['newCall'] = newCall;
                vCustom['final'] = '';

                if(v.duration > 0){ // pris direct = bien ou delta d'appel de 300s
                    //d['background'] = 'table-success';
                    //d['etat'] = 'Pris direct 👍';
                    vCustom['final'] = 'call_direct';
                    vCustom['bg'] = 'table-success';
                    newCall = true;
                    vCustom['newCall'] = newCall;
                    //++;
                }
                
                if(tel1 == v.num_from){ // le numero est deja en boucle en n-1, on compte le nombre de fois
                    loop++;
                    var delta = moment(v.creationDatetime).diff(time) / 1000;
                    //moreTd = '<td>Tour '+loop+' après '+delta+' sec.';
                    if(delta > 60){ // le premier tour arrive 60sec apres l'appel = nouvel appel = DIRECT IN
                        wait = 0;
                        loop = 0;
                        newCall = true;
                        vCustom['newCall'] = newCall; 
                        if(v.duration == 0){ // appel non pris apres le delta de 60"
                            //loop = 0;
                            vCustom['hangupAfterSeconds'] = wait;
                            vCustom['final'] = '';//'appel_entrant_delta>60+duration=0';
                            
                            //vCustom['bg'] = 'table-success';
                        } else { // appel pris apres le delai de 60"
                            //moreTd = '<td>start';
                            //loop = 0; // reinit du comptage
                            vCustom['state'] = 'start';
                            vCustom['final'] = 'call_direct';
                            vCustom['hangupAfterSeconds'] = wait;
                            //vCustom['bg'] = 'table-success';
                        }
                    } else {
                        wait += delta;
                    

                    // on peut enregistrer l'etat du comptage ici
                    vCustom['state'] = 'transfert';
                    vCustom['start'] = 'loop';
                    vCustom['nbLoop'] = loop;
                    newCall = false;
                    vCustom['newCall'] = newCall;
                    vCustom['hangupAfterSeconds'] = wait;
                    if(v.duration > 0 && !newCall){
                        //d['background'] = 'table-info';
                        //d['etat'] = 'Pris en transfert';
                        vCustom['bg'] = 'table-info';
                        vCustom['final'] = 'call_transfert';
                        //transfert++;
                        //moreTd += '<br><b>appel pris après '+wait+' sec.</b>';
                        vCustom['hangupAfterSeconds'] = wait;
                        if(loop == 0){ // si la boucle est a 0 = rappel juste apres avoir raccroché -> direct
                            vCustom['final'] = 'call_direct';
                            vCustom['bg'] = 'table-success';
                            wait = 0; // on reinit le temps d'attente de decrochage
                            vCustom['hangupAfterSeconds'] = wait;
                            newCall = true;
                            vCustom['newCall'] = newCall;
                        }

                        
                    } }
                    //moreTd += '</td>';
                } else if (tel2 == v.num_from) { // on test la position du numero en n-2
                    vCustom['final'] = 'le n-2 est identique'
                } else { //if (tel1 != v.calling) {
                    //console.log('le numero n-1 nest pas le meme = je le met à jour');
                    tel1 = v.num_from;
                    tel2 = v.num_from;
                    duration2 = v.duration;
                    wait = 0;
                    loop = 0;
                } // else if (tel2 != v.calling) {
                //     console.log('le numero n-2 nest pas le meme = je le met à jour');
                //     tel2 = v.calling;
                //     duration1 = v.duration;
                //     wait = 0;
                //     loop = 0;
                // }

                //$('#loading').hide();
                //$('#content #datas .table tbody').append('<tr class="'+d.background+'"><th scope="row">'+i+' - '+moment(v.creationDatetime).format('DD MMM YYYY \à HH:mm:ss')+'</th><td>'+v.calling+'</td><td>'+v.duration+' sec.</td><td>'+v.wayType+'</td><td>'+v.called+'</td><td>'+d['etat']+'</td>'+moreTd+'</tr>');
                
                time = v.creationDatetime;

                delete vCustom['destinationType'];
                delete vCustom['designation'];
                delete vCustom['countrySuffix'];
                delete vCustom['planType'];
                delete vCustom['priceWithoutTax'];

                // verification sur l'appel non répondu
                if(loop > 0 && v.duration == 0){ // la duration est nulle + tour de boucle > 0
                    idLinePrevious = i; 
                // } else if (v.newCall && listeCallsCustom[idLinePrevious]['calling'] == listeCallsCustom[i]['calling']) {
                //     // si l'appel en cours est nouveau && le même numéro que la ligne d'avant
                //     listeCallsCustom[idLinePrevious]['final'] = 'le meme num a deja raccroché';
                } else if (loop == 0 && newCall){ // c'est un nouvel appel
                    //console.log('idLinePrevious : '+idLinePrevious+' / i courant : '+i+' => la ligne '+idLinePrevious+ 'doit etre en rouge');
                    if(idLinePrevious > 0 && (i - idLinePrevious) == 1){ // le noeud 0 ne peux pas etre un echec ! il a au moijs tourner une fois
                        //console.log('dans le if 318');
                        listeCallsCustom[idLinePrevious]['final'] = 'call_fail';
                        listeCallsCustom[idLinePrevious]['bg'] = 'table-danger';
                    }
                // } else if ( v.final == "repondeur" && (i - idLinePrevious) == 1){
                //     listeCallsCustom[idLinePrevious]['final'] = 'svi_fail_to_rep';
                //     listeCallsCustom[idLinePrevious]['bg'] = '';
                // }
                // } else {
                //     listeCallsCustom[idLinePrevious]['final'] = '';
                //     listeCallsCustom[idLinePrevious]['bg'] = '';
                 }

                

            }
            listeCallsCustom.push(vCustom);
        });

        console.log('listeCallsCustomAAAA',listeCallsCustom);

        // traitement des lignes poiur la gestion des messages
        idLinePrevious = 0;
        $.each(listeCallsCustom, function (i, v) {
            if(v.type == "message"){ // le type de l'occurence est un message !
                idLinePrevious = i; // l'id precédent est enregistré
            }

            if(!v.include && v.type == 'call'){
                listeCallsCustom[i]['final'] = 'svi_fail';
                listeCallsCustom[i]['bg'] = 'table-primary';
            }

            // correctif pour les appels pris en transfert qui on le tour 0 en svi_fail !
            if(v['state'] == "transfert" && listeCallsCustom[i -1]['num_from'] == v['num_from']){
                if(listeCallsCustom[i - 1]['nbLoop'] == 0){ // 
                    listeCallsCustom[i - 1]['final'] = "";
                    listeCallsCustom[i -1]['bg'] = '';
                }
            }

            if(idLinePrevious > 0 && (i - idLinePrevious) == 1){
                var j =(idLinePrevious-1);
                listeCallsCustom[j]['bg'] = 'table-danger';
                //listeCallsCustom[(idLinePrevious-1)]['final'] = 'i:'+i+' <br>idLinePrevious:'+idLinePrevious+'<br>message_next';
                if(listeCallsCustom[j]['nbLoop'] == 0){
                    listeCallsCustom[j]['final'] = "message_svi_fail"
                } else {
                    listeCallsCustom[j]['final'] = "message_call_fail"
                }
            }

            if(listeCallsCustom[listeCallsCustom.length - 1]['type'] == "message"){
                if(listeCallsCustom[listeCallsCustom.length - 2]['nbLoop'] == 0){ // message direct du svi
                    listeCallsCustom[listeCallsCustom.length - 2]['final'] = "message_svi_fail"
                } else {
                    listeCallsCustom[listeCallsCustom.length - 2]['final'] = "message_call_fail"
                }
            }
        })

        // gestion de la derniere ligne du tableau
        var last = listeCallsCustom[(listeCallsCustom.length-1)]
        //console.log(last);
        if(last.duration == 0 && last.nbLoop > 0){ // c'est un transfert non répondu !
            last['final'] = "call_fail";
            last['bg'] = 'table-danger';
        } else if(last.duration > 0 && last.nbLoop > 0){
            last['final'] = 'call_transfert';
            last['bg'] = 'table-info';
        }

        var direct = 0;
        var transfert = 0;
        var fail = 0;
        var repondeur = 0;
        var svi_fail = 0;
        var message_svi_fail = 0;
        var message_call_fail = 0;
        var wait_time = 0;
        var wait_time_call_fail = 0;
        var wait_time_svi_fail = 0;
        var wait_time_message_svi_fail = 0;

        console.log('listeCallsCustom', listeCallsCustom);

        $.each(listeCallsCustom, function (i, v) {
            //if(v.wayType == 'incoming'){ // seulement les appels entrants
                // var moreTd = '<td>tour '+v.nbLoop;
                // if(v.hangupAfterSeconds > 0){
                //     //moreTd += '<br><b>Appel pris après '+v.hangupAfterSeconds+' sec.</b>';
                //     moreTd += '</td><td>'+v.hangupAfterSeconds;
                // } else {
                //     moreTd += '</td><td>0';
                // }
                // moreTd += '</td>';

                if(v.final == 'call_fail'){
                    fail++;
                    wait_time_call_fail += v.hangupAfterSeconds;
                } else if(v.final == 'call_direct'){
                    direct++;
                } else if(v.final == 'call_transfert'){
                    transfert++;
                    wait_time += v.hangupAfterSeconds;
                } else if(v.final == 'svi_fail'){
                    svi_fail++;
                    wait_time_svi_fail += v.duration;
                } else if(v.final == 'repondeur'){
                    repondeur++;
                } else if(v.final == 'message_svi_fail'){
                    message_svi_fail++;
                    wait_time_message_svi_fail += v.duration;
                } else if(v.final == 'message_call_fail'){
                    message_call_fail++;
                }

                var icon = '<i class="fas fa-voicemail"></i>';

                if(v.type == 'call'){
                    icon = '<i class="fas fa-phone"></i>';
                }
                // ok $('#content #datas .table tbody').append('<tr class="'+v.bg+'"><th scope="row">'+i+' - '+moment(v.creationDatetime).format('DD MMM YYYY \à HH:mm:ss')+'</th><td>'+v.calling+'</td><td>'+v.duration+' sec.</td><td>'+v.wayType+'</td><td>'+v.called+'</td><td>'+v.state+'</td>'+moreTd+'<td>'+v.newCall+'</td></tr>');
                $('#content #datas .table tbody').append('<tr class="'+v.bg+'"><th scope="row">'+icon+'</th><th scope="row">'+moment(v.creationDatetime).format('YYYY-MM-DD')+'</th><td>'+moment(v.creationDatetime).format('HH:mm:ss')+'</td><td>'+v.num_from+'</td><td>'+v.duration+'</td><td>'+v.num_to+'</td><td>'+v.state+'</td><td>'+v.nbLoop+'</td><td>'+v.hangupAfterSeconds+'</td><td>'+v.final+'</td></tr>');
            //}
        });
        
        $('#loading').hide(); 

        var full_fail = fail + message_call_fail;

        var total_calls = direct + transfert + full_fail + svi_fail + message_svi_fail;
        var total_messages = message_svi_fail + message_call_fail;

        $('#content #datas #jumbotron').html('<h3>'+total_calls +' appels entrants entre '+datas['from']+ ' et '+datas['to'] + '<br>Taux de décroché global : '+percentage(direct+transfert,direct+transfert+full_fail)+'%</h3>');
        $('#content #datas #jumbotron').append('<h4>dont '+direct+' appels décrochés direct ('+percentage(direct,total_calls)+'%)</h4>');
        $('#content #datas #jumbotron').append('<h4>dont '+transfert+' décroché après transfert ('+percentage(transfert,total_calls)+'%) . <small>attente moyenne: '+Math.round((wait_time/transfert))+'"</small></h4>');
        $('#content #datas #jumbotron').append('<h4>dont '+full_fail+' non répondus ('+percentage(full_fail,total_calls)+'%) <small>et '+percentage(message_call_fail,full_fail)+'% des ces appels ont laissés un message vocal / temps moyen d’abandon: '+Math.round((wait_time_call_fail/fail))+'"</small></h4>');
        $('#content #datas #jumbotron').append('<h4>dont '+svi_fail+' abandonnés avant transfert sur les lignes ou répondeur ('+percentage(svi_fail,total_calls)+'%) . <small>temps moyen d’abandon: '+Math.round((wait_time_svi_fail/svi_fail))+'"</small></h4>');
        $('#content #datas #jumbotron').append('<h4>dont '+message_svi_fail+' qui a laissé un message car les lignes n\'étaient pas dispo/ouvertes  ('+percentage(message_svi_fail,total_calls)+'%)</h4>');
        $('#content #datas #jumbotron').append('<h3>'+total_messages +' messages laissés entre '+datas['from']+ ' et '+datas['to'] + '</h3>');
        $('#content #datas #jumbotron').append('<h4>dont '+message_svi_fail+' messages laissés directement en arrivant ('+percentage(message_svi_fail,total_messages)+'%) . <small>delai moyen de dépot du message : '+Math.round((wait_time_message_svi_fail/message_svi_fail))+'"</small></h4>');
        $('#content #datas #jumbotron').append('<h4>dont '+message_call_fail+' messages laissés après attente sur les lignes ('+percentage(message_call_fail,total_messages)+'%)</h4>');

    }



    $( "#interval" ).delegate( "#callsAndMessages", "click", function() {
        getCallsAndMessages();
    });


    // recuperation asynchrone des appels vers l'api
    function getCalls (datas) {
        return $.ajax({
            url : url+'/lines/'+datas['number']+'/calls',
            data: {"from": datas['datetimeFrom'],"to":datas['datetimeTo'], "display":datas['display']},
            type : 'GET'
        });
    }

    function getMessages (datas) {
        return $.ajax({
            url : url+'/lines/'+datas['number']+'/messages',
            data: {"from": datas['datetimeFrom'],"to":datas['datetimeTo'], "display":datas['display']},
            type : 'GET'
        });
    }

    // percentage
    function percentage(value, total){
        return Math.round((value/total)*100);
    }
});