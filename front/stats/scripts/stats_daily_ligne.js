import * as env from"./env.js";
$(function() {
    const url = env.baseUrl;
    var datas = [];

    $("#top_menu").hide();


    //$('#content').append('<div class="navbar" id="lines"></div>');
    //$('#content').append('<div id="datas"><h3></h3><h2></h2><pre></pre></div>');


    $('#loading').hide();
    

    $("#from").val(moment().subtract(1, 'days').format("YYYY/MM/DD")+" 00:00");
    $("#to").val(moment().subtract(1, 'days').format("YYYY/MM/DD")+" 23:59");


    

    $('#from').datetimepicker({
        inline:false,
        timepicker:false,
        format:'Y/m/d H:i',
        minDate:'2019/12/01',
        maxDate: moment().subtract(1, 'days').format("YYYY/MM/DD")
    });

    $( "#from" ).change(function() {
        var from = $("#from").val();
        $("#to").val(from.split(' ')[0]+" 23:59");
      });

    // function de traitement des appels recus
    function traitement (datas){

        var stats = datas.stats;

        var tr = stats['call_direct'] + stats['call_transfert'];
        var ttr = stats['call_direct'] + stats['call_transfert'] + stats['call_fail'];
        var ttd = ttr + stats['call_ring_but_no_hangup'];

        $('#content').append(`<div class="stats container mt-4">
    <div class="row">
        <div class="col px-2 mx-1 border rounded">
            <h3 class="text-center border-bottom p-3">Taux de réponse</h3>
            <h1 class="display-1 text-center">${percentage(tr, ttr)}%</h1>
            <p style="line-height: 0.8em"><small>
                <strong>C'est la part des demandes adressées sur la ligne ayant fait l'objet d'une réponse.</strong><br />
                Si un appel est adressé plusieurs fois de suite sur la même ligne, l'outil ne compte qu'une seule demande.<br />
                <em>Exemple : une demande est présentée 3 fois sur la ligne et décrochée la 3ème fois, le taux de réponse est 100 %.</em>
            </small></p>
        </div>
        <div class="col px-2 mx-1 border rounded">
            <h3 class="text-center border-bottom p-3">Taux de décroché</h3>
            <h1 class="display-1 text-center">${percentage(tr, ttd)}%</h1>
            <p style="line-height: 0.8em"><small>
                <strong>C'est la part des sollicitations adressées sur la ligne ayant fait l'objet d'une réponse.</strong><br />
                Si un appel est adressé plusieurs fois de suite sur la même ligne, l'outil compte chaque sollicitation.<br />
                <em>Exemple : une demande est présentée 3 fois sur la ligne et décrochée la 3ème fois, le taux de réponse est 33 % (1 décroché sur 3 sollicitations).</em>
            </small></p>
        </div>
    </div>
</div>`);

        datas['from'] = $("#from").val()
        datas['to'] = $("#to").val()


        $('#content #datas').html(`<table class="table">
        <thead>
        <tr>
            <th scope="col"></th>
            <th scope="col">svi open</th>
            <th scope="col">Date</th>
            <th scope="col">Heure</th>
            <th scope="col">Appelant</th>
            <th scope="col">membre?</th>
            <th scope="col">Durée (s)</th>
            <th scope="col">Vers</th>
            <th scope="col">Etat</th>
            <th scope="col">state</th>
            <th scope="col">Attente (s)</th>
            <th scope="col">final</th>
        </tr>
        </thead>
        <tbody></tbody>
        </table>`);

    
        $.each(datas.datas, function (i, v) {
            var icon = '<i class="fas fa-voicemail"></i>';

            if(v.type == 'call'){
                icon = '<i class="fas fa-phone"></i>';
            }

            var border = 'border-danger';
            if(v.officeIsOpen == true){
                border = 'border-success'; 
            }
            $('#content #datas .table tbody').append('<tr class="'+v.bg+' border-left '+border+'" style="border-width: 10px!important;"><th scope="row">'+icon+'</th><td>'+v.officeIsOpen+'</td><th scope="row">'+moment(v.creationDatetime).format('YYYY-MM-DD')+'</th><td>'+moment(v.creationDatetime).format('HH:mm:ss')+'</td><td>'+v.num_from+'</td><td>'+v.isMember+'</td><td>'+v.duration+'</td><td>'+v.num_to+'</td><td>'+v.state+'</td><td>'+v.nbLoop+'</td><td>'+v.hangupAfterSeconds+'</td><td>'+v.final+'</td></tr>');
        });
        
        $('#loading').hide(); 
    }



    $( "#interval" ).delegate( "#callsAndMessages", "click", function() {
        var from = $("#from").val().replace(/\//gi,'-').replace(/ /gi,'T');
        var to = $("#to").val().replace(/\//gi,'-').replace(/ /gi,'T');
        $('#content').html('');
        $('#loading').show();
        $.ajax({
            url : url+'/stats',
            type : 'GET',
            data: {"from": from,"to": to, "numero": "0033374096724", "sviopen": true},
            statusCode: {
                200: function (res) {
                    traitement(res);
                }
            },
            error: function (request, status, error) {
                console.log('error');
            }
        });
    });

    // percentage
    function percentage(value, total){
        return Math.round((value/total)*100);
    }
});