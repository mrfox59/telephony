import * as env from"./env.js";
$(function() {

    // var environnement = localStorage.getItem('env');
    // // on charge PRD par defaut
    // if(environnement){
    //     // charge celui présent en localstorage
    //     //$('select').find().attr('selected','selected');
    //     //$('select#selectEnv option[value="'+environnement+'"]').prop('selected', true);
    //     console.log('load !!', environnement);
    //     loadEnv(environnement);
    //     //var environnement = localStorage.getItem('env');
    // } else {
    //     // sinon par defaut la prd
    //     loadEnv('prd');
    //     location.reload();
    // }


    // au chargement, je stocke la version courante en local storage
    localStorage.setItem("version",env.version)

    // fonction de test de disponibilité du backend
    function getStatusBackend () {
        //console.log("getStatusBackend");
        $.ajax({
            url : env.baseUrl+'/me',
            dataType: 'json',
            type : 'GET',
            success : function(res, status){
                $('#backend_status').removeClass('badge-danger');
                $('#backend_status').html('Backend '+res.version);
                //console.log('version env.js :',res.version)
                //console.log('version locale :',localStorage.getItem("version"))
                // if(res.front[0].version.trim() != localStorage.getItem("version").trim()){
                //     $.bootstrapGrowl("Une nouvelle version est disponible, <a href='' onclick='location.reload(true) return true;'>cliquez ici pour rafraichir</a> !", {
                //         ele: 'body', 
                //         type: 'info', 
                //         delay: 8000,
                //         align: 'right',
                //         width: 'auto', 
                //         allow_dismiss: false,
                //         stackup_spacing: 10 // espace entre les alertes
                //     });
                // }
            },
            error: function (error) {
                console.log('backend DOWN !')
                $('#backend_status').addClass('badge-danger');
            }
        });
    }

    getStatusBackend();
    setInterval(getStatusBackend, 10000);



	// top menu global
    $("#top_menu").append(`<nav class="navbar navbar-expand-lg navbar-light bg-light">
                            <a class="navbar-brand" href="#">TELEPHONIE UALRT</a>
                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation"> 
                                <span class="navbar-toggler-icon"></span> 
                            </button>
                            <div class="collapse navbar-collapse" id="navbarText">
                                <ul id="menuAfterLogin" class="navbar-nav mr-auto"></ul> 
                                <span class="navbar-text">${env.version} du ${env.lastUpdate}</span>&nbsp;&nbsp;
                                <a href="${env.baseUrl}/hello" target="_blank" title="Cliquez pour votre rendre sur la console d\'administration du backend">
                                    <span id="backend_status" class="badge badge-success">Backend</span>
                                </a>
                                
                            </div>
                           </nav>`)
    
    // ajout des items pricipaux
    $("#menuAfterLogin").append('<li class="nav-item"><a class="nav-link" href="index.html">Home</a></li>');
    $("#menuAfterLogin").append('<li class="nav-item"><a class="nav-link" href="stats_daily.html">Stats du jour</a></li>');
    $("#menuAfterLogin").append('<li class="nav-item"><a class="nav-link" href="stats_daily_ws.html">Stats précédentes</a></li>');
    $("#menuAfterLogin").append('<li class="nav-item"><a class="nav-link" href="stats_full.html">Comptes rendus</a></li>');
    $("#menuAfterLogin").append('<li class="nav-item"><a class="nav-link" href="stats_multi.html">Comptes rendus multi</a></li>');
    // $("#menuAfterLogin").append('<li class="nav-item"><a class="nav-link" href="applications.html">Applications</a></li>');
    // $("#menuAfterLogin").append('<li class="nav-item dropdown"><a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Catalizr</a><div class="dropdown-menu" aria-labelledby="navbarDropdown" id="dropdown_catalizr"></div></li>');
    
    // ajout du dropdown catalizr
    // $("#dropdown_catalizr").append('<a class="dropdown-item" href="catalizr_banks.html">Banques</a>');
    // $("#dropdown_catalizr").append('<a class="dropdown-item" href="catalizr_operations.html">Investissements</a>');
    // $("#dropdown_catalizr").append('<a class="dropdown-item" href="catalizr_advisor.html">Comptes Advisor</a>');
    // $("#dropdown_catalizr").append('<a class="dropdown-item" href="catalizr_surveys.html">Sondages</a>');


    //$('select#selectEnv').val(environnement).trigger('change');

    // selection de l'environnement
    // $( "body" ).delegate( "select#selectEnv", "change", function() {
    //     var environnement = $(this).val();
    //     loadEnv(environnement)
    //     location.reload();   
    // });

    // function loadEnv(environnement) {
    //     $.post( env.baseUrl+'/env', 
    //         { 
    //             environnement: environnement
    //         }
    //     )
    //     .done(function( data ) {
    //         localStorage.setItem("env",environnement);
    //     })
    //     .fail(function(data ) {
    //         console.log('et merde ca pete sa mere !')
    //     });
    // }
});