import * as env from"./env.js";
$(function() {
    const url = env.baseUrl;
    //let listeCalls = [];
    let listeCallsId = [];
    //let listeCallsAndMessages = [];
    var datas = [];

    // ajout de la ligne des boutons de sondes
    $('#content').append('<div class="navbar" id="lines"></div>');
    $('#content').append('<div id="datas"><h3></h3><h2></h2><pre></pre></div>');


    $('#loading').hide();
    

    $("#from").val(moment().format("YYYY/MM/DD")+" 00:00");
    $("#to").val(moment().format("YYYY/MM/DD")+" 23:59");

    

    $('#from, #to').datetimepicker({
        inline:false,
        format:'Y/m/d H:i'
    });

    // function de traitement des appels recus
    function traitement (listeCalls){

        datas['from'] = $("#from").val()
        datas['to'] = $("#to").val()

        var direct = 0;
        var transfert = 0;
        var fail = 0;
        var repondeur = 0;
        var call_svi_fail = 0;
        var message_svi_fail = 0;
        var message_call_fail = 0;
        var wait_time = 0;
        var wait_time_call_fail = 0;
        var wait_time_svi_fail = 0;
        var wait_time_message_svi_fail = 0;

        var outgoing_call = 0;
        var outgoing_duration = 0;

        $('#loading').hide();

        $('#content #datas').html(`<div id="jumbotron">
      </div>
        <table class="table">
        <thead>
        <tr>
            <th scope="col"></th>
            <th scope="col">svi open</th>
            <th scope="col">Date</th>
            <th scope="col">Heure</th>
            <th scope="col">Appelant</th>
            <th scope="col">membre?</th>
            <th scope="col">Durée (s)</th>
            <th scope="col">Vers</th>
            <th scope="col">Etat</th>
            <th scope="col">state</th>
            <th scope="col">Attente (s)</th>
            <th scope="col">final</th>
        </tr>
        </thead>
        <tbody></tbody>
        </table>`);



        $.each(listeCalls, function (i, v) {
            if(v.final == 'call_fail'){
                fail++;
                wait_time_call_fail += Number(v.hangupAfterSeconds);
            } else if(v.final == 'call_direct'){
                direct++;
            } else if(v.final == 'call_transfert'){
                transfert++;
                wait_time += Number(v.hangupAfterSeconds);
            } else if(v.final == 'call_svi_fail'){
                call_svi_fail++;
                wait_time_svi_fail += Number(v.duration);
            } else if(v.final == 'repondeur'){
                repondeur++;
            } else if(v.final == 'message_svi_fail'){
                message_svi_fail++;
                wait_time_message_svi_fail += Number(v.duration);
            } else if(v.final == 'message_call_fail'){
                message_call_fail++;
            } else if(v.final == 'outgoing'){
                outgoing_call++;
                outgoing_duration += Number(v.duration);
            }

            var icon = '<i class="fas fa-voicemail"></i>';

            if(v.type == 'call'){
                icon = '<i class="fas fa-phone"></i>';
                if(v.wayType == 'outgoing'){
                    icon = '<i class="fas fa-headset"></i>';
                }
            }

            var border = 'border-danger';
            if(v.officeIsOpen == true){
                border = 'border-success'; 
            }
            // ok $('#content #datas .table tbody').append('<tr class="'+v.bg+'"><th scope="row">'+i+' - '+moment(v.creationDatetime).format('DD MMM YYYY \à HH:mm:ss')+'</th><td>'+v.calling+'</td><td>'+v.duration+' sec.</td><td>'+v.wayType+'</td><td>'+v.called+'</td><td>'+v.state+'</td>'+moreTd+'<td>'+v.newCall+'</td></tr>');
            $('#content #datas .table tbody').append('<tr class="'+v.bg+' border-left '+border+'" style="border-width: 10px!important;"><th scope="row">'+icon+'</th><td>'+v.officeIsOpen+'</td><th scope="row">'+moment(v.creationDatetime).format('YYYY-MM-DD')+'</th><td>'+moment(v.creationDatetime).format('HH:mm:ss')+'</td><td>'+v.num_from+'</td><td>'+v.isMember+'</td><td>'+v.duration+'</td><td>'+v.num_to+'</td><td>'+v.state+'</td><td>'+v.nbLoop+'</td><td>'+v.hangupAfterSeconds+'</td><td>'+v.final+'</td></tr>');
            
        });
        
        $('#loading').hide(); 

        var full_fail = fail + message_call_fail;

        var total_calls = direct + transfert + full_fail + call_svi_fail + message_svi_fail;
        var total_messages = message_svi_fail + message_call_fail;
        $('#content #datas #jumbotron').html('<h3>'+total_calls +' appels entrants entre '+datas['from']+ ' et '+datas['to'] + '</h3>');
        $('#content #datas #jumbotron').append('<h4>dont '+direct+' appels décrochés direct ('+percentage(direct,total_calls)+'%)</h4>');
        $('#content #datas #jumbotron').append('<h4>dont '+transfert+' décroché après transfert ('+percentage(transfert,total_calls)+'%) . <small>attente moyenne: '+Math.round((wait_time/transfert))+'"</small></h4>');
        $('#content #datas #jumbotron').append('<h4>dont '+full_fail+' non répondus ('+percentage(full_fail,total_calls)+'%) <small>et '+percentage(message_call_fail,full_fail)+'% des ces appels ont laissés un message vocal / temps moyen d’abandon: '+Math.round((wait_time_call_fail/fail))+'"</small></h4>');
        $('#content #datas #jumbotron').append('<h4>dont '+call_svi_fail+' abandonnés avant transfert sur les lignes ou repondeur ('+percentage(call_svi_fail,total_calls)+'%) . <small>temps moyen d’abandon: '+Math.round((wait_time_svi_fail/call_svi_fail))+'"</small></h4>');
        $('#content #datas #jumbotron').append('<h4>dont '+message_svi_fail+' qui a laissé un message car les lignes n\'étaient pas dispo/ouvertes  ('+percentage(message_svi_fail,total_calls)+'%)</h4>');
        $('#content #datas #jumbotron').append('<h3>'+total_messages +' messages laissés entre '+datas['from']+ ' et '+datas['to'] + '</h3>');
        $('#content #datas #jumbotron').append('<h4>dont '+message_svi_fail+' messages laissés directement en arrivant ('+percentage(message_svi_fail,total_messages)+'%) . <small>delai moyen de dépot du message : '+Math.round((wait_time_message_svi_fail/message_svi_fail))+'"</small></h4>');
        $('#content #datas #jumbotron').append('<h4>dont '+message_call_fail+' messages laissés après attente sur les lignes ('+percentage(message_call_fail,total_messages)+'%)</h4>');
        $('#content #datas #jumbotron').append('<hr>');
        $('#content #datas #jumbotron').append('<h3>'+outgoing_call+' appels sortants - total '+outgoing_duration+'"</h3>');
    }



    $( "#interval" ).delegate( "#callsAndMessages", "click", function() {
        //var url = 'http://localhost:8080';
        var from = $("#from").val().replace(/\//gi,'-').replace(/ /gi,'T');
        var to = $("#to").val().replace(/\//gi,'-').replace(/ /gi,'T');
        $.ajax({
            url : url+'/stats',
            type : 'GET',
            data: {"from": from,"to": to},
            statusCode: {
                200: function (res) {
                    traitement(res.datas);
                }
            },
            error: function (request, status, error) {
                console.log('error');
            }
        });
    });

    // percentage
    function percentage(value, total){
        return Math.round((value/total)*100);
    }
});